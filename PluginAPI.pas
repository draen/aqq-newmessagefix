unit PluginAPI;

interface

uses Windows;

/// Hooks
const
  // Funkcja: TAK wParam=PPluginNewsData lParam=0 Res=0
  // Opis: Dodaje nowe �r�d�o powiadomie�.
  AQQ_SYSTEM_NEWSSOURCE_ADD = 'AQQ/System/NewsSource/Add';
  // Notyfikacja: TAK wParam=PWideChar (�r�d�o) lParam=0 Res=0
  // Opis: Informuje o usuni�ciu �r�d�a powiadomie�.
  // Funkcja: TAK wParam=PPluginNewsData lParam=0 Res=[-1 b��d; 0 ok]
  // Opis: Usuwa �r�d�o powiadomie� o podanym ID.
  AQQ_SYSTEM_NEWSSOURCE_DELETE = 'AQQ/System/NewsSource/Delete';
  // Funkcja: TAK wParam=PPluginNewsData lParam=0 Res=[-1 b��d; 0 ok]
  // Opis: Uaktualnia dane o �r�dle powiadomie�.
  AQQ_SYSTEM_NEWSSOURCE_UPDATE = 'AQQ/System/NewsSource/Update';
  // Notyfikacja: TAK wParam=PWideChar (�r�d�o) lParam=[0 nieaktywne; 1 aktywne] Res=[-1 b��d; 0 ok]
  // Opis: Informuje o zmianie stanu aktywno�ci �r�d�a przez u�ytkownika.
  AQQ_SYSTEM_NEWSSOURCE_ACTIVE = 'AQQ/System/NewsSource/Active';
  // Funkcja: TAK wParam=PPluginNewsData lParam=0 Res=[-1 b��d; 0 ok]
  // Opis: Od�wie�a �r�d�o powiadomie�.
  AQQ_SYSTEM_NEWSSOURCE_REFRESH = 'AQQ/System/NewsSource/Refresh';
  // Notyfikacja: TAK wParam=0 lParam=0 Res=0
  // Opis: Informuje o rozpocz�ciu procesu synchronizacji �r�de� powiadomie�.
  AQQ_SYSTEM_NEWSSOURCE_BEFOREFETCH = 'AQQ/System/NewsSource/BeforeFetch';
  // Notyfikacja: TAK wParam=Integer (ID) lParam=PWideChar (�r�d�o) Res=[0 nie dotyczy; -1 b��d; 1 ok]
  // Opis: Wskazane �r�d�o powiadomie� jest proszone o pobranie danych. wParam zawiera wew. ID �r�d�a powiadomie�.
  AQQ_SYSTEM_NEWSSOURCE_FETCH = 'AQQ/System/NewsSource/Fetch';
  // Funkcja: TAK wParam=Integer (wew. ID) lParam=0 Res=0
  // Opis: Informujemy o rozpocz�ciu pobierania danych dla �r�d�a. ID pochodzi z notyfikacji AQQ_SYSTEM_NEWSSOURCE_FETCH.
  AQQ_SYSTEM_NEWSSOURCE_FETCHSTART = 'AQQ/System/NewsSource/FetchStart';
  // Funkcja: TAK wParam=Integer (wew. ID) lParam=[0 ok; 1 nie uda�o si� pobra� danych] Res=[1 ok; 0 b��d]
  // Opis: Informujemy o zako�czeniu pobierania danych dla �r�d�a. ID pochodzi z notyfikacji AQQ_SYSTEM_NEWSSOURCE_FETCH.
  AQQ_SYSTEM_NEWSSOURCE_FETCHEND = 'AQQ/System/NewsSource/FetchEnd';
  // Funkcja: TAK wParam=PPluginNewsItem lParam=0 Res=0
  // Opis: Przekazujemy dane na temat pobranego powiadomienia.
  AQQ_SYSTEM_NEWSSOURCE_ITEM = 'AQQ/System/NewsSource/Item';
  // Notyfikacja: TAK wParam=0 lParam=PWideChar (jid) Res=PPluginAccountInfo
  // Opis: AQQ odpytuje o informacje na temat konta do wy�wietlenia w oknie zmiany opisu.
  AQQ_SYSTEM_GETACCOUNTINFO = 'AQQ/System/GetAccountInfo';
  // Notyfikacja: TAK wParam=PPluginContact lParam=PPluginPopUp Res=0
  // Opis: Dotyczy popmenu kontaktu na li�cie, kt�re ma si� pojawi� za chwil�.
  AQQ_SYSTEM_POPUP = 'AQQ/System/Popup';
  // Funkcja: TAK wParam=0 lParam=PPluginChatPresence Res=0
  // Opis: Przekazujemy powiadomienie na temat aktywno�ci kontaktu na czacie.
  AQQ_SYSTEM_CHAT_PRESENCE = 'AQQ/System/Chat/Presence';
  // Funkcja: TAK wParam=0 lParam=PPluginChatOpen Res=0
  // Opis: Otwieramy nowe okno pokoju rozm�w.
  AQQ_SYSTEM_CHAT_OPEN = 'AQQ/System/Chat/Open';
  // Notyfikacja: TAK wParam=0 lParam=PPluginSong Res=[0 nie dotyczy; 1 ok]
  // Opis: Zwracamy piosenk�/utw�r kt�ry jest aktualnie odgrywany.
  AQQ_SYSTEM_CURRENTSONG = 'AQQ/System/CurrentSong';
  // Notyfikacja: TAK wParam=PPluginContact lParam=PPluginChatState Res=[0 ok; 1 nast�pi�y zmiany w lParam]
  // Opis: Informuje o pisaniu przez u�ytkownika wiadomo�ci tekstowej.
  // Funkcja: TAK wParam=PPluginContact lParam=PPluginChatState Res=[0 b��d; 1 ok]
  // Opis: Zmiana w funkcji "pisaka" dla wskazanego rozm�wcy.
  AQQ_SYSTEM_MSGCOMPOSING = 'AQQ/System/MsgComposing';
  // Funkcja: TAK wParam=0 lParam=PPluginError Res=[0 okno nie znalezione; 1 ok]
  // Opis: Informujemy o powsta�ym b��dzie (w protokole wtyczki sieciowej) - w oknie rozmowy z kontaktem.
  AQQ_SYSTEM_ERROR = 'AQQ/System/Error';
  // Funkcja: TAK wParam=PPluginHook lParam=0 Res=0
  // Opis: Wywo�ujemy dowoln� wskazan� funkcje / notyfikacje w obszarze SDK dla innych wtyczek.
  AQQ_SYSTEM_SENDHOOK = 'AQQ/System/SendHook';
  // Notyfikacja: TAK wParam=0 lParam=0 Res=0
  // Opis: Informuje wtyczk� o za�adowaniu si� wszystkich dost�pnych innych wtyczek.
  // Funkcja: TAK wParam=0 lParam=0 Res=[0 wtyczki nie za�adowane; 1 wtyczki za�adowane]
  // Opis: Odpytuj� czy wszystkie wtyczki zosta�y przez AQQ za�adowane.
  AQQ_SYSTEM_MODULESLOADED = 'AQQ/System/ModulesLoaded';
  // Funkcja: TAK wParam=0 lParam=PPluginSMSGate Res=Integer (Nadane ID bramki)
  // Opis: Funkcja dodaje now� bramk� SMS do zak�adki SMS�w w oknie g��wnym AQQ.
  AQQ_SYSTEM_ADDSMSGATE = 'AQQ/System/AddSMSGate';
  // Funkcja: TAK wParam=Integer (ID bramki) lParam=0 Res=[0 b��d; 1 ok]
  // Opis: Funkcja usuwa dodan� bramk� SMS - nale�y poda� ID bramki uzyskane przy jej dodawaniu.
  AQQ_SYSTEM_REMOVESMSGATE = 'AQQ/System/RemoveSMSGate';
  // Funkcja: TAK wParam=PWideChar (nowa nazwa) lParam=Integer (ID bramki) Res=[0 b��d; 1 ok]
  // Opis: Funkcja zmienia nazw� dodanej bramki SMS.
  AQQ_SYSTEM_RENAMESMSGATE = 'AQQ/System/RenameSMSGate';
  // Notyfikacja: TAK wParam=0 lParam=0 Res=0
  // Opis: Informuje wtyczk�, �e o ile jest sieciowa, powinna przedstawi� list� dost�pnych agent�w na serwerze (muc, wyszukiwarka, rejestracja, etc.)
  AQQ_SYSTEM_GETAGENTS = 'AQQ/System/GetAgents';
  // Funkcja: TAK wParam=0 lParam=PPluginAgent Res=[1 agent ju� istnieje i zostanie zaktualizowany; 0 agent zostanie dodany]
  // Opis: Funkcja edytuje lub dodaje nowego agenta serwera Jabberowego (lub jakiegokolwiek innego zgodnego ze standardami).
  AQQ_SYSTEM_SETAGENT = 'AQQ/System/SetAgent';
  // Notyfikacja: TAK wParam=PWideChar (jid agenta) lParam=Integer (sta�a sprawdzaj�ca ##013 #0014) Res=[0 agent aktywny; 1 agent nie aktywny]
  // Opis: AQQ odpytuje, czy dany agent na serwerze obs�uguj� w danym momencie akcje. Akcja oznaczona jest sta�� sprawdzaj�c�, np. AQQ_QUERY_MESSAGE.
  AQQ_SYSTEM_ISAGENTENABLED = 'AQQ/System/IsAgentEnabled';
  // Funkcja: TAK wParam=PWideChar (jid agenta) lParam=0 Res=[1 usuni�to; 0 nie usuni�to/nie znaleziono agenta]
  // Opis: Funkcja usuwa dodanego wcze�niej agenta.
  AQQ_SYSTEM_REMOVEAGENT = 'AQQ/System/RemoveAgent';
  // Notyfikacja: TAK wParam=0 lParam=0 Res=[0 agent aktywny; 1 agent nie aktywny]
  // Opis: AQQ informuje, �e wtyczka sieciowa - o ile obs�uguj� import/eksport kontakt�w - powinna o tym poinformowa� AQQ za pomoc� AQQ_SYSTEM_ADDIMPEXP (ka�dorazowo, po ka�dym takim zapytaniu).
  AQQ_SYSTEM_GETIMPEXP = 'AQQ/System/GetImpExp';
  // Funkcja: TAK wParam=0 lParam=PPluginImpExp Res=0
  // Opis: Funkcja dodaje serwis importu/eksportu kontakt�w lub innych akcji zwi�zanych z list� kontakt�w, kt�re obs�uguje wtyczka sieciowa.
  AQQ_SYSTEM_ADDIMPEXP = 'AQQ/System/AddImpExp';
  // Notyfikacja: TAK wParam=PPluginContact lParam=Integer (sta�a sprawdzaj�ca ##013 ##014) Res=[0 zgoda/lub nie dotyczy; 1 akcja zablokowana]
  // Opis: AQQ odpytuje, czy dla wskazanego kontaktu, mo�na wykona� akcje oznaczon� sta�� sprawdzaj�c� (np. AQQ_QUERY_SENDFILE).
  AQQ_SYSTEM_QUERY = 'AQQ/System/Query';
  // Notyfikacja: TAK wParam=PPluginStateChange lParam=Integer (sta�a sprawdzaj�ca ##013 ##014) Res=[0 b��d; 1 wype�niono PPluginStateChange]
  // Opis: AQQ prosi o wype�nienie struktury sprawdzaj�cej stan danego konta. Sta�a sprawdzaj�ca w tym wypadku, to zazwyczaj zawsze AQQ_QUERY_NETWORKSTATE.
  AQQ_SYSTEM_QUERYEX = 'AQQ/System/QueryEx';
  // Notyfikacja: TAK wParam=PWideChar (numer telefonu) lParam=0 Res=[0 brak obs�ugi; Numeryczne ID bramki obs�uguj�cej nr. telefonu]
  // Opis: AQQ prosi wtyczk� o odpowied�, kt�ra z wew. bramek SMS, obs�uguje wskazany numer telefonu (o ile jakakolwiek to robi).
  AQQ_SYSTEM_SMSSUPPORTED = 'AQQ/System/SMSSupported';
  // Notyfikacja: TAK wParam=Integer (wew. ID bramki) lParam=0 Res=
  // Opis: U�ytkownik uruchomi� konfigurator wskazanej bramki SMS.
  AQQ_SYSTEM_SMSCONFIG = 'AQQ/System/SMSConfig';
  // Notyfikacja: TAK wParam=PPluginSMSResult lParam=PPluginSMS Res=Integer (sta�a wyniku wysy�ki SMS ##021)
  // Opis: U�ytkownik wysy�a wiadomo�� SMS.
  AQQ_SYSTEM_SMSSEND = 'AQQ/System/SMSSend';
  // Notyfikacja: TAK wParam=Integer (0 domy�lnie; 1 sta�e ##004) lParam=PPluginStateChange Res=0
  // Opis: AQQ prosi o zmian� stanu wtyczki sieciowej, wed�ug podanych danych w strukturze PPluginStateChange.
  AQQ_SYSTEM_SETNOTE = 'AQQ/System/SetNote';
  // Notyfikacja: TAK wParam=0 lParam=PPluginWindowEvent Res=0
  // Opis: AQQ informuj� o zmianie stanu jednego z otwartych okien.
  AQQ_SYSTEM_WINDOWEVENT = 'AQQ/System/WindowEvent';
  // Notyfikacja: TAK wParam=0 lParam=PPluginStateChange Res=0
  // Opis: AQQ informuj� o zmianie stanu jednego z kont sieciowych.
  AQQ_SYSTEM_STATECHANGE = 'AQQ/System/StateChange';
  // Notyfikacja: TAK wParam=0 lParam=0 Res=0
  // Opis: AQQ prosi wtyczki sieciowe, o nazwi�zanie po��czenia z zapami�tanym ostatnio stanem (przy starcie programu).
  AQQ_SYSTEM_SETLASTSTATE = 'AQQ/System/SetLastState';
  // Funkcja: TAK wParam=Integer (0 domy�lnie; 1 wymu� reset po��czenia) lParam=PWideChar (nowa nazwa zasobu) Res=0
  // Opis: Funkcja zmienia tre�� zasob�w dla wszystkich sieci Jabber. Mo�liwy jest te� restart po��czenia.
  AQQ_SYSTEM_CHANGE_JABBERRESOURCES = 'AQQ/System/Change/Resources';
  // Funkcja: TAK wParam=0 lParam=PWideChar (nazwa akcji) Res=[0 b��d; 1 akcja uruchomiona]
  // Opis: Funkcja uruchamia jedn� z wew. akcji dost�pnych w AQQ (np. aSettings uruchomi ustawienia AQQ).
  AQQ_SYSTEM_RUNACTION = 'AQQ/System/RunAction';
  // Funkcja: TAK wParam=PWideChar (XML) lParam=Integer (ID konta) Res=[0 wys�ano; 1 b��dne ID; 2 konto roz��czone]
  // Opis: Funkcja wysy�a pakiet XML poprzez jedno ze wskazanych kont Jabberowych.
  AQQ_SYSTEM_SENDXML = 'AQQ/System/SendXML';
  // Notyfikacja: TAK wParam=PWideChar (XML) lParam=PPluginXMLChunk Res=[0 nie dotyczy; 1 pakiet zosta� obs�u�ony]
  // Opis: Podgl�da pakiet�w Jabberowych zawieraj�cych pole ID. Wtyczka mo�e wybi�rczo obs�u�y� kt�rykolwiek z tych pakiet�w.
  AQQ_SYSTEM_XMLIDDEBUG = 'AQQ/System/XMLIDDebug';
  // Notyfikacja: TAK wParam=PWideChar (XML) lParam=PPluginXMLChunk Res=[0 nie dotyczy; 1 pakiet zosta� obs�u�ony]
  // Opis: Podgl�da pakiet�w Jabberowych. Wtyczka mo�e wybi�rczo obs�u�y� kt�rykolwiek z tych pakiet�w.
  AQQ_SYSTEM_XMLDEBUG = 'AQQ/System/XMLDebug';
  // Notyfikacja: TAK wParam=PPluginContact lParam=Integer (sta�a online ##018) Res=[0 nie dotyczy; 1 konto po��czone + zgoda; 2 konto roz��czone + brak zgody; 3 brak zogdy]
  // Opis: AQQ odpytuje wtyczk� sieciow� czy dla wskazanego kontaktu, mo�e nast�pi� akcja oznaczon� sta�� online (np. ONCHECK_DELETE).
  AQQ_SYSTEM_ONLINECHECK = 'AQQ/System/OnlineCheck';
  // Funkcja: TAK wParam=0 lParam=PPluginDebugInfo Res=0
  // Opis: Funkcja dodaje pakiet XML do konna konsili XML.
  AQQ_SYSTEM_DEBUG_XML = 'AQQ/System/Debug/XML';
  // Notyfikacja: TAK wParam=0 lParam=0 Res=0
  // Opis: Wtyczka sieciowa po otrzymaniu tej notyfikacji, powinna si� natychmiast roz��czy� z serwerem.
  AQQ_SYSTEM_DISCONNECT = 'AQQ/System/Disconnect';
  // Notyfikacja: TAK wParam=0 lParam=PPluginAutomation Res=0
  // Opis: AQQ informuj�, �e auto-oddalenie zosta�o aktywowane.
  AQQ_SYSTEM_AUTOMATION_AUTOAWAY_ON = 'AQQ/System/Automation/AutoAway/On';
  // Notyfikacja: TAK wParam=0 lParam=0 Res=0
  // Opis: AQQ informuj�, �e auto-oddalenie zosta�o deaktywowane.
  AQQ_SYSTEM_AUTOMATION_AUTOAWAY_OFF = 'AQQ/System/Automation/AutoAway/Off';
  // Funkcja: TAK wParam=0 lParam=0 Res=0
  // Opis: Wtyczka sieciowa, zaraz po po��czeniu powinna wywo�a� t� funkcje. AQQ przez kolejne 10s nie b�dzie informowa� o �adnych zmianach stanu u�ytkownik�w na li�cie.
  AQQ_SYSTEM_ONCONNECT_SILENCE = 'AQQ/System/OnConnect/Silence';
  // Notyfikacja: TAK wParam=0 lParam=PWideChar (JID) Res=[PWideChar �cie�ka do pliku graficznego; 0 nie dotyczy]
  // Opis: O ile wskazany kontakt pochodzi z odpytywanej wtyczki, powinna ona zwr�ci� �cie�k� do pliku graficznego skojarzonego z aktualnym stanem kontaktu.
  AQQ_SYSTEM_GETCURRENTSHOWTYPE_PATH = 'AQQ/System/GetCurrentShowType/Path';
  // Notyfikacja: TAK wParam=PPluginSmallInfo lParam=PWideChar (domena) Res=[0 nie dotyczy; PWidechar UID]
  // Opis: AQQ prosi o zwrot, lub wype�nienie mini struktury aktualnym UID-em, skojarzonym ze wskazan� domen�.
  AQQ_SYSTEM_GETCURRENTUID = 'AQQ/System/GetCurrentUID';
  // Notyfikacja: TAK wParam=[0 brak obramowania; 1 obramowanie aktywne; 2 styl natywny Windows] lParam=PWideChar (�cie�ka do aktywnej kompozycji wizualnej) Res=0
  // Opis: AQQ informuj�, �e aktywna kompozycja wizualna zmieni�a si�.
  AQQ_SYSTEM_THEMECHANGED = 'AQQ/System/ThemeChanged';
  // Notyfikacja: TAK wParam=[0 brak obramowania; 1 obramowanie aktywne; 2 styl natywny Windows] lParam=PWideChar (�cie�ka do aktywnej kompozycji wizualnej) Res=0
  // Opis: AQQ informuj� przy starcie, jaka aktualnie wybrana jest kompozycja wizualna.
  AQQ_SYSTEM_THEMESTART = 'AQQ/System/ThemeStart';
  // Notyfikacja: TAK wParam=0 lParam=0 Res=[0; nie dotyczy; TResultEx = array of Integer gdzie poszczeg�lne warto�ci zawieraj� wska�niki na PPluginMaxStatus]
  // Opis: Wtyczka mo�e poda� informacje odtycz�ce nazw obs�ugiwanych kont, indeks�w ikon, i max. d�ugo�ci obs�ugiwanego statusu opisowego. Dane umiszczone zostan� w oknie zmiany stanu.
  AQQ_SYSTEM_MAXSTATUSLENGTH = 'AQQ/System/MaxStatusLength';
  // Notyfikacja: TAK wParam=0 lParam=PPluginContact Res=[0; nie dotyczy; Integer max. d�ugo�� wiadomo�ci tekstowej]
  // Opis: AQQ odpytuje wtyczk� sieciow�, o max. d�ugo�� wiadomo�ci tekstowej jak� mo�na wys�a� do wskazanego kontaktu.
  AQQ_SYSTEM_MAXMSGLENGTH = 'AQQ/System/MaxMsgLength';
  // Funkcja: TAK wParam=PWideChar (nazwa pliku bez �cie�ki) lParam=PWideChar (JID kontaktu) Res=PWideChar wygenerowana �cie�ka do pliku
  // Opis: Tworzy now� �cie�k�, zawieraj�c� zmodyfikowan� wskazan� nazw� pliku, kt�rej mo�na u�y� przy zapisie pliku. Stosowane np. w trakcie odbierania obrazk�w prosto do okna rozmowy.
  AQQ_SYSTEM_GETNEWCACHEITEMPATH = 'AQQ/System/GetNewCacheItemPath';
  // Notyfikacja: TAK wParam=0 lParam=PWideChar (dane) Res=[0; nie dotyczy; 1 wtyczka obs�u�y�a]
  // Opis: Obs�uga URL Protocol. Notyfikacja wywo�ywana gdy jaka� akcja z ekploratora Windows zostanie przekazana do AQQ (np. skojarzenie z plikami .aqq).
  AQQ_SYSTEM_PERFORM_COPYDATA = 'AQQ/System/Perform/CopyData';
  // Notyfikacja: TAK wParam=Integer (sta�a stanu) lParam=0 Res=0
  // Opis: AQQ prosi wtyczk� sieciow� o zmian� stanu konta. Wtyczka powinna si� podporz�dkowa�.
  AQQ_SYSTEM_FORCESTATUS = 'AQQ/System/ForceStatus';
  // Notyfikacja: TAK wParam=0 lParam=PPluginContact Res=[0 nie dotyczy; PWideChar - JID/UID]
  // Opis: AQQ prosi wtyczk� o wskazanie, co powinno znale�� si� w schowku Windows, po u�yciu funkcji kopiuj JID/UID (dla wskazanego kontaktu)
  AQQ_SYSTEM_CLIPBOARD_JID = 'AQQ/System/Clipboard/JID';
  // Notyfikacja: TAK wParam=0 lParam=Integer (sta�a zak�adek ##003) Res=0
  // Opis: AQQ informuje kt�ra z zak�adek w oknie g��wnym jest obecnie aktywna.
  AQQ_SYSTEM_TABCHANGE = 'AQQ/System/TabChange';
  // Funkcja: TAK wParam=Integer (ID konta) lParam=PWideChar (XML) Res=0
  // Opis: Interpretuje pakiet XML (tak jak by dotar� on prosta z serwera) dla wskazanego konta Jabberowego.
  AQQ_SYSTEM_INTERPRET_XML = 'AQQ/System/Interpret/XML';
  // Notyfikacja: TAK wParam=0 lParam=PPluginTransfer Res=[0 nie dotyczy; 1 ok]
  // Opis: Informuje o zmianie stanu transferu pliku (np. odrzucenie).
  // Funkcja: TAK wParam=0 lParam=PPluginTransfer Res=0
  // Opis: Aktualizuje stan transferu pliku.
  AQQ_SYSTEM_TRANSFER_STATUS_CHANGE = 'AQQ/System/Transfer/Status/Change';
  // Notyfikacja: TAK wParam=0 lParam=PPluginAutomation Res=0
  // Opis: AQQ informuje o automatycznym przej�ciu w stan zabezpieczenia.
  AQQ_SYSTEM_AUTOMATION_AUTOSECURE = 'AQQ/System/Automation/AutoSecure';
  // Notyfikacja: TAK wParam=0 lParam=0 Res=0
  // Opis: AQQ zosta�o zabezpieczone.
  AQQ_SYSTEM_AUTOSECURE_ON = 'AQQ/System/AutoSecure/On';
  // Notyfikacja: TAK wParam=0 lParam=0 Res=0
  // Opis: AQQ zosta�o odbezpieczone.
  AQQ_SYSTEM_AUTOSECURE_OFF = 'AQQ/System/AutoSecure/Off';
  // Notyfikacja: TAK wParam=0 lParam=PPluginTriple (Handle1 uchwyt okna; Handle2 uchwyt pola; Handle3 uchwyt menu; Param1 x; Param2 y] Res=0
  // Opis: Informuje o tym, �e za chwil� pojawi si� menu skojarzone z danym polem tekstowym.
  AQQ_SYSTEM_MSGCONTEXT_POPUP = 'AQQ/System/OnMsgContent/Popup';
  // Notyfikacja: TAK wParam=0 lParam=PPluginTriple (Handle1 uchwyt okna; Handle2 uchwyt pola; Handle3 uchwyt menu; Param1 x; Param2 y] Res=0
  // Opis: Informuje o tym, �e menu skojarzone z danym polem tekstowym zosta�o zamkni�te.
  AQQ_SYSTEM_MSGCONTEXT_CLOSE = 'AQQ/System/OnMsgContent/Close';
  // Funkcja: TAK wParam=Integer (sta�a stanu wiadomo�ci sms ##020) lParam=PWideChar (ID SMS-a) Res=0
  // Opis: Aktualizuje status wiadomo�ci SMS (np. SMS_STATUS_DELIVERED).
  AQQ_SYSTEM_CHANGESMS_STATUS = 'AQQ/System/ChangeSMS/Status';
  // Funkcja: TAK wParam=PWideChar (stare ID wiadomo�ci SMS) lParam=PWideChar (nowe ID wiadomo�ci SMS) Res=0
  // Opis: Aktualizuje ID wiadomo�ci SMS (zmienia je na nowe).
  AQQ_SYSTEM_CHANGESMS_ID = 'AQQ/System/ChangeSMS/ID';
  // Notyfikacja: TAK wParam=0 lParam=PPluginToolTipID Res=[0 ok; 1 blokada tooltipa]
  // Opis: Informuje, �e zaraz pojawi si� tooltip kontaktu na li�cie. Mo�na go teraz uzupe�ni� o dodatkowe dane.
  AQQ_SYSTEM_TOOLTIP_FILL = 'AQQ/System/ToolTip/Fill';
  // Notyfikacja: TAK wParam=PPluginTriple (Param1 typ dodatku TAddonType Param2 dodatek aktywny po instalacji 0/1] lParam=PWideChar (�cie�ka do dodatku je�eli mo�liwe jest jej podanie) Res=0
  // Opis: Informuje o tym, �e dodatek zosta� w�a�nie zainstalowany.
  AQQ_SYSTEM_ADDONINSTALLED = 'AQQ/System/AddonInstalled';
  // Notyfikacja: TAK wParam=0 lParam=0 Res=[0 ok; 1 blokada]
  // Opis: Informuje, �e zaraz pojawi si� tooltip kontaktu na li�cie. Mo�na go zablokowa�.
  AQQ_SYSTEM_TOOLTIP_BEFORESHOW = 'AQQ/System/ToolTip/BeforeShow';
  // Funkcja: TAK wParam=0 lParam=PPluginToolTipItem Res=0
  // Opis: Dodaje nowe pole do tooltipa kontaktu na li�cie.
  AQQ_SYSTEM_TOOLTIP_ADDITEM = 'AQQ/System/ToolTip/AddItem';
  // Funkcja: TAK wParam=Integer [delay 0, lub wi�cej w milisekundach] lParam=0 Res=0
  // Opis: Pokazuje przygotowany ostatnio tooltip.
  AQQ_SYSTEM_TOOLTIP_SHOW = 'AQQ/System/ToolTip/Show';
  // Notyfikacja: TAK wParam=0 lParam=PWideChar (JID) Res=[0 nie dotyczy; Integer sta�a po��czenia np. CONNSTATE_CONNECTING ##008]
  // Opis: AQQ pyta o stan po��czenia z sieci� skojarzon� ze wskazanym kontaktem.
  AQQ_SYSTEM_CONNECTION_STATE = 'AQQ/System/Connection/State';
  // Funkcja: TAK wParam=0 lParam=PWideChar (�cie�ka do nowej kompozycji wizualnej) Res=0
  // Opis: Zmiania aktywn� kompozycje wizualn� (ale nie aktywuje jej!)
  AQQ_SYSTEM_THEME_SET = 'AQQ/System/Theme/Set';
  // Funkcja: TAK wParam=0 lParam=[0 szybki refresh; 1 od�wie�a wszystko] Res=0
  // Opis: Aktywuje wybran� kompozycje wizualn�.
  AQQ_SYSTEM_THEME_APPLY = 'AQQ/System/Theme/Apply';
  // Funkcja: TAK wParam=0 lParam=0 Res=0
  // Opis: Od�wie�a list� kontakt�w wzgl�dem aktywnej kompozycji wizualnej.
  AQQ_SYSTEM_THEME_REFRESH = 'AQQ/System/Theme/Refresh';
  // Funkcja: TAK wParam=0 lParam=PPluginChatPrep Res=0
  // Opis: Otwiera/Tworzy czat na kanale.
  AQQ_SYSTEM_CHAT = 'AQQ/System/Chat';
  // Funkcja: TAK wParam=0 lParam=PPluginStateChange Res=0
  // Opis: Zmienia status i stan wszystkich kont.
  AQQ_SYSTEM_SETSHOWANDSTATUS = 'AQQ/System/SetShowAndStatus';
  // Funkcja: TAK wParam=[0 dodanie; 1 usuni�cie] lParam=PWideChar (odno�nik URL) Res=[0 dodano; 1 usuni�to]
  // Opis: Dodaje, b�d� usuwa odno�niki URL z kt�rych AQQ pobiera informacje na temat dost�pnych aktualizacji.
  AQQ_SYSTEM_SETUPDATELINK = 'AQQ/System/SetUpdateLink';
  // Funkcja: TAK wParam=Integer (sta�a funkcyjna ##002) lParam=[0 wy��czono; 1 w��czono] Res=0
  // Opis: W��cza lub wy��cza niekt�re funkcje zwi�zane z dzia�aniem AQQ. Np. SYS_FUNCTION_SEARCHONLIST
  AQQ_SYSTEM_FUNCTION_SETENABLED = 'AQQ/System/SetEnabled';
  // Funkcja: TAK wParam=Integer (0 je�eli dodajemy; ID uzyskane poprzednim razem je�eli edytujemy/usuwamy) lParam=PPluginAccountEvents Res=Integer (ID)
  // Opis: Ustala, jakie dzia�ania s� obs�ugiwane we wtyczce sieciowej wzgl�dem konta i dodaje je na list� kont wtyczek sieciowych w ustawieniach AQQ. Aby usun�� resetujemy wszystkie pola w PPluginAccountEvents, i ustalamy tylko ID.
  AQQ_SYSTEM_ACCOUNT_EVENTS = 'AQQ/System/Account/Events';
  // Notyfikacja: TAK wParam=Integer (ID) lParam=Integer (sta�a akcji konta ##001) Res=0
  // Opis: AQQ uruchamia wskazan� akcje dla wskazanego konta. Np. ACCOUNT_EVENT_CHANGEPASS
  AQQ_SYSTEM_ACCOUNT_RUNEVENT = 'AQQ/System/Account/RunEvent';
  // Funkcja: TAK wParam=0 lParam=0 Res=0
  // Opis: Restartuje AQQ.
  AQQ_SYSTEM_RESTART = 'AQQ/System/Restart';
  // Funkcja: TAK wParam=[0; bez wskazania; PWideChar �cie�ka do wtyczki, spowoduje od�wie�enie tylko jej] lParam=[0 od�wie�a wtyczki; 1 od�wie�a wtyczki r�wnie� te niewczytane] Res=0
  // Opis: Od�wie�a list� wtyczek w ustawieniach AQQ.
  AQQ_SYSTEM_PLUGIN_REFRESHLIST = 'AQQ/System/Plugin/RefreshList';
  // Funkcja: TAK wParam=[0; dodajemy do listy 1 usuwamy z listy; 2 sprawdzmy czy wtyczka jest na li�cie] lParam=PWideChar (�ci�ka do wtyczki) Res=[0 ok; 1 wtyczka wy��czona (przy sprawdzaniu)]
  // Opis: Dodaje wtyczk� do listy wtyczek wykluczonych.
  AQQ_SYSTEM_PLUGIN_EXCLUDE = 'AQQ/System/Plugin/Exclude';
  // Funkcja: TAK wParam=[0; w��czamy wtyczk�; 1 wy��czamy wtyczk�; 2 sprawdzamy czy wtyczka aktywna] lParam=PWideChar (�ci�ka do wtyczki) Res=[Integer uchwyt do wtyczki; 0 tylko przy wy��czeniu wtyczki]
  // Opis: W��cza lub wy��cza wtyczk�. Mo�na sprawdzi� te�, czy dana wtyczka jest aktualnie w��czona.
  AQQ_SYSTEM_PLUGIN_ACTIVE = 'AQQ/System/Plugin/Active';
  // Funkcja: TAK wParam=Integer (sta�a d�wi�ku ##022) lParam=[0 domy�lnie; 1 wymusza odegranie d�wi�ku] Res=0
  // Opis: Odgrywa wybrany d�wi�k.
  AQQ_SYSTEM_PLAYSOUND = 'AQQ/System/Plugin/PlaySound';
  // Notyfikacja: TAK wParam=0 lParam=PWideChar (Adres URL) Res=PWideChar [0 nie robi nic; PWideChar nowy adres URL)
  // Opis: Informuj�, �e otwierany jest wew. browser dodatk�w dla AQQ. Notyfikacja podaje adres URL kt�ry zostanie otwarty. Mo�na go zmieni� zwracaj�c nowy adres URL do notyfikacji.
  AQQ_SYSTEM_ADDONBROWSER_URL = 'AQQ/System/AddonBrowser/URL';
  // Funkcja: TAK wParam=[1; start rozmowy; 2 koniec rozmowy] lParam=[Handle uchwyt okna rozmowy] Res=0
  // Opis: Informuj� AQQ, �e rozpoczyna si� lub ko�czy rozmowa w ramach sieci skype (audio b�d� video).
  AQQ_SYSTEM_SKYPE_CONVSERSATION = 'AQQ/System/Skype/Conversation';

  /// Windows - okna
  // Notyfikacja: TAK wParam=0 lParam=0 Res=PWideChar (status tekstowy)
  // Opis: Przed pokazaniem si� okna ze zmian� stanu, o ile dotyczy ono wtyczki sieciowej, powinna ona zwr�ci� aktualnie ustawiony opis - kt�ry zostanie umieszczony w otwartym oknie.
  AQQ_WINDOW_SETNOTE_PUTNOTE = 'AQQ/Window/SetNote/PutNote';
  // Notyfikacja: TAK wParam=0 lParam=0 Res=PWideChar (JID)
  // Opis: Przed pokazaniem si� okna ze zmian� stanu, o ile dotyczy ono wtyczki sieciowej, powinna ona zwr�ci� JID konta kt�rego dotyczy przysz�a zmiana stanu.
  AQQ_WINDOW_SETNOTE_NOTEJID = 'AQQ/Window/SetNote/NoteJID';
  // Notyfikacja: TAK wParam=PPluginWindowStatus lParam=0 Res=Integer (sta�a zmiany statusu w oknie ##011)
  // Opis: Przed zmian� stanu za pomoc� okna zmiany stanu/opisu, wtyczka jest o tym informowana. Jej odpowied� (jedna z dost�pnych sta�ych) b�dzie mia�a wp�yw na dalsze losy ustalania opisu.
  AQQ_WINDOW_SETSTATUS = 'AQQ/Window/SetStatus';
  // Notyfikacja: TAK wParam=Integer (sta�a stanu) lParam=PWideChar (status tekstowy) Res=[0 ok; 1 blokada zmiany stanu]
  // Opis: Przestarza�a notyfikacja - nie nale�y stosowa�. Informuje o zmianie stanu (podaje nowy stan i opis).
  AQQ_WINDOW_PRESETNOTE_NOTE = 'AQQ/Window/PreSetNote/Note';
  // Notyfikacja: TAK wParam=0 lParam=0 Res=0
  // Opis: Okno ze zmian� stanu zosta�o zamkni�te.
  AQQ_WINDOW_SETNOTE_CLOSE = 'AQQ/Window/SetNote/Close';

const
  /// Paski narz�dziowe
  // Funkcja: TAK wParam=0 lParam=PPluginAction Res=-1
  // Opis: Funkcja wykorzystywana do tworzenia, niszczenia, i aktualizacji element�w znajduj�cych si� na paskach narz�dziowych.
        // Wiadomo�� AQQ_CONTROLS_TOOLBAR nigdy nie jest wysy�ana tylko i wy��cznie we w�asnej przestrzeni nazw tj. "AQQ/Controls/Toolbar/".
        // Prawid�owe wys�anie wiadomo�ci, nast�puje przy stworzeniu konkretnej  przestrzeni nazw, wed�ug podanego wzorca:
        // AQQ_CONTROLS_TOOLBAR + [Nazwa paska narz�dziowego] + AQQ_CONTROLS_CREATEBUTTON
        // Tego typu wywo�anie tworzy nowy element.
        // AQQ_CONTROLS_TOOLBAR + [Nazwa paska narz�dziowego] + AQQ_CONTROLS_DESTROYBUTTON
        // Tego typu wywo�anie niszczy element.
        // AQQ_CONTROLS_TOOLBAR + [Nazwa paska narz�dziowego] + AQQ_CONTROLS_UPDATEBUTTON
        // Tego typu wywo�anie aktualizuj� element.
  AQQ_CONTROLS_TOOLBAR = 'AQQ/Controls/Toolbar/';
  // Funkcja: TAK wParam=0 lParam=0 Res=0
  // Opis: Cz�� przestrzeni nazw wykorzystywanej przy funkcji AQQ_CONTROLS_TOOLBAR.
  AQQ_CONTROLS_CREATEBUTTON = '/CreateButton';
  // Funkcja: TAK wParam=0 lParam=0 Res=0
  // Opis: Cz�� przestrzeni nazw wykorzystywanej przy funkcji AQQ_CONTROLS_TOOLBAR.
  AQQ_CONTROLS_DESTROYBUTTON = '/DestroyButton';
  // Funkcja: TAK wParam=0 lParam=0 Res=0
  // Opis: Cz�� przestrzeni nazw wykorzystywanej przy funkcji AQQ_CONTROLS_TOOLBAR.
  AQQ_CONTROLS_UPDATEBUTTON = '/UpdateButton';

  /// Kontrolki IE
  // Funkcja: TAK wParam=[0 - okno g��wne X - uchwyt do okna rozmowy lub innego okna] lParam=PPluginWebBrowser Res=[0 b��d; Integer uchwyt do kontrolki IE]
  // Opis: Funkcja s�u�y do tworzenia, lub aktualizacji kontrolki typu TWebBrowser.
  AQQ_CONTROLS_WEBBROWSER_CREATE = 'AQQ/Controls/WebBrowser/Create';
  // Funkcja: TAK wParam=PwideChar (adres URL) lParam=PPluginWebBrowser Res=[0 b��d; 1 ok]
  // Opis: Funkcja s�u�y do nawigacji na now� stron� we wskazanej kontrolce TWebBrowser.
  AQQ_CONTROLS_WEBBROWSER_NAVIGATE = 'AQQ/Controls/WebBrowser/Navigate';
  // Notyfikacja: TAK wParam=0 lParam=Integer (uchwyt kontrolki IE) Res=0
  // Opis: Informuje o zako�czonym wczytywaniu strony HTML.
  AQQ_CONTROLS_WEBBROWSER_NAVCOMPLETE = 'AQQ/Controls/WebBrowser/NavComplete';
  // Funkcja: TAK wParam=PPluginWebItem lParam=PPluginWebBrowser (pole handle ustawione na 1 wskazywa� b�dzie na list� kontakt�w) Res=[0 b��d; 1 ok]
  // Opis: Funkcja s�u�y zmiany zawarto�ci elementu o podanym ID w kodzie strony HTML.
  AQQ_CONTROLS_WEBBROWSER_SETID = 'AQQ/Controls/WebBrowser/SetID';
  // Funkcja: TAK wParam=PPluginWebItem lParam=PPluginWebBrowser (pole handle ustawione na 1 wskazywa� b�dzie na list� kontakt�w) Res=[0 b��d; 1 ok]
  // Opis: Funkcja pobiera zawarto�� elementu o podanym ID w kodzie strony HTML.
  AQQ_CONTROLS_WEBBROWSER_GETID = 'AQQ/Controls/WebBrowser/GetID';
  // Notyfikacja: TAK wParam=0 lParam=PPluginWebBeforeNavEvent Res=0
  // Opis: Informuje o rozpocz�ciu wczytywania nowej strony HTML.
  AQQ_CONTROLS_WEBBROWSER_BEFORENAV = 'AQQ/Controls/WebBrowser/BeforeNav';
  // Notyfikacja: TAK wParam=PwideChar (nowy status) lParam=Integer (uchwyt kontrolki IE) Res=0
  // Opis: Informuje o zmianie statusu w oknie kontrolki IE.
  AQQ_CONTROLS_WEBBROWSER_STATUSCHANGE = 'AQQ/Controls/WebBrowser/StatusChange';
  // Notyfikacja: TAK wParam=PwideChar (nowy tytu�) lParam=Integer (uchwyt kontrolki IE) Res=0
  // Opis: Informuje o zmianie tytu�u w oknie kontrolki IE.
  AQQ_CONTROLS_WEBBROWSER_TITLECHANGE = 'AQQ/Controls/WebBrowser/TitleChange';
  // Funkcja: TAK wParam=0 lParam=PPluginWebBrowser Res=[0 b��d; 1 ok]
  // Opis: Funkcja s�u�y niszczenia wskazanej kontrolki typu TWebBrowser.
  AQQ_CONTROLS_WEBBROWSER_DESTROY = 'AQQ/Controls/WebBrowser/Destroy';
  // Funkcja: TAK wParam=PPluginWebItem (pole Text powinno by� puste) lParam=PPluginWebBrowser Res=[0 b��d; 1 ok]
  // Opis: Funkcja s�u�y do wywo�ania wirtualnego klikni�cia na element o wskazanym ID.
  AQQ_CONTROLS_WEBBROWSER_CLICKID = 'AQQ/Controls/WebBrowser/ClickID';

  /// Menu
  // Funkcja: TAK wParam=0 lParam=PPluginItemDescriber Res=[0 b��d; PPluginAction]
  // Opis: Funkcja s�u�y do pobierania danych na temat elementu znajduj�cego si� na wskazanym komponencie PopUp (menu).
  AQQ_CONTROLS_GETPOPUPMENUITEM = 'AQQ/Controls/GetPopupMenuItem';
  // Funkcja: TAK wParam=0 lParam=PPluginAction Res=0
  // Opis: Funkcja s�u�y do tworzenia nowego menu (typu PopUp).
  AQQ_CONTROLS_EDITPOPUPMENUITEM = 'AQQ/Controls/EditPopUpMenuItem';
  // Funkcja: TAK wParam=0 lParam=PPluginActionEdit Res=0
  // Opis: Funkcja s�u�y do uaktualnienia stanu elementu w menu (typu PopUp).
  AQQ_CONTROLS_CREATEPOPUPMENU = 'AQQ/Controls/CreatePopUpMenu';
  // Funkcja: TAK wParam=0 lParam=PPluginAction Res=[0 b��d; 1 ok]
  // Opis: Funkcja s�u�y do tworzenia nowego elementu na wskazanym menu typu PopUp.
  AQQ_CONTROLS_CREATEPOPUPMENUITEM = 'AQQ/Controls/CreatePopUpMenuItem';
  // Funkcja: TAK wParam=0 lParam=PPluginAction Res=0
  // Opis: Funkcja s�u�y do niszczenia wskazanego menu (typu PopUp).
  AQQ_CONTROLS_DESTROYPOPUPMENU = 'AQQ/Controls/DestroyPopUpMenu';
  // Funkcja: TAK wParam=0 lParam=PPluginAction Res=0
  // Opis: Funkcja s�u�y do niszczenia wskazanego elementu w menu typu PopUp.
  AQQ_CONTROLS_DESTROYPOPUPMENUITEM = 'AQQ/Controls/DestroyPopUpMenuItem';

  /// Inne kontrolki
  // Funkcja: TAK wParam=0 lParam=PwideChar (nowy status) Res=0
  // Opis: Funkcja zmienia status na dolnym panelu w g��wnym oknie rozmowy.
  AQQ_CONTROLS_MAINSTATUS_SETPANELTEXT = 'AQQ/Controls/MainStatus/SetPanelText';
  // Funkcja: TAK wParam=0 lParam=[0 wy��cz; 1 w��cz] Res=0
  // Opis: W��cza lub wy��cza elementy w oknie g��wnym powi�zane z zak�adk� SMS.
  AQQ_CONTROLS_SMSCONTROLS_ENABLE = 'AQQ/Controls/SMSControls/Enable';

  /// Kontakty
  // Notyfikacja: TAK wParam=0 lParam=0 Res=0
  // Opis: Informuje o zako�czeniu �adowania listy kontakt�w przy starcie AQQ.
  AQQ_CONTACTS_LISTREADY = 'AQQ/Contacts/ListReady';
  // Notyfikacja: TAK wParam=PwideChar (jid) lParam=0 Res=[0 nie; 1 tak]
  // Opis: AQQ odpytuje, czy wskazany JID pochodzi z kt�rej� wtyczki sieciowej.
  AQQ_CONTACTS_FROMPLUGIN = 'AQQ/Contacts/FromPlugin';
  // Funkcja: TAK wParam=0 lParam=PPluginContactSimpleInfo Res=[0 brak kontaktu; 1 dane uzupe�nione z pliku; 2 dane uzupe�nione]
  // Opis: Funkcja uzupe�nia w podanej strukturze dane na temat wskazanego kontaktu (JID).
  AQQ_CONTACTS_FILLSIMPLEINFO = 'AQQ/Contacts/FillSimpleInfo';
  // Funkcja: TAK wParam=0 lParam=PPluginContactSimpleInfo Res=0
  // Opis: Funkcja zmienia dane na temat wskazanego kontaktu (JID).
  AQQ_CONTACTS_SET_SIMPLEINFO = 'AQQ/Contacts/Set/SimpleInfo';
  // Funkcja: TAK wParam=0 lParam=PWideChar (jid) Res=PPluginContactSimpleInfo
  // Opis: Funkcja wyparta przez AQQ_CONTACTS_FILLSIMPLEINFO. Pobiera dane na temat wskazanego kontaktu.
  AQQ_CONTACTS_GET_SIMPLEINFO = 'AQQ/Contacts/Get/SimpleInfo';
  // Notyfikacja: TAK wParam=0 lParam=PWideChar (jid) Res=0
  // Opis: Informuje wtyczk� o dodaniu nowego bana dla wskazanego JID-u.
  // Funkcja: TAK wParam=0 lParam=PWideChar (jid) Res=0
  // Opis: Dodaj� JID do listy zbanowanych kontakt�w.
  AQQ_CONTACTS_ADD_BAN = 'AQQ/Contacts/Add/Ban';
  // Notyfikacja: TAK wParam=0 lParam=PWideChar (jid) Res=0
  // Opis: Informuje wtyczk� o usuni�ciu bana dla wskazanego JID-u.
  // Funkcja: TAK wParam=0 lParam=PWideChar (jid) Res=0
  // Opis: Usuwa JID z listy zbanowanych kontakt�w.
  AQQ_CONTACTS_REMOVE_BAN = 'AQQ/Contacts/Remove/Ban';
  // Funkcja: TAK wParam=0 lParam=PWideChar (jid) Res=[0 kontakt bez bana; 1 kontakt ma bana]
  // Opis: Sprawdza czy dany JID jest na li�cie zablokowanych.
  AQQ_CONTACTS_HAVE_BAN = 'AQQ/Contacts/Have/Ban';
  // Notyfikacja: TAK wParam=0 lParam=PPluginAddUser Res=[0 je�eli nie dotyczy wtyczki; 1 je�eli wtyczka doda�a kontakt]
  // Opis: Informuje wtyczk� o dodaniu nowego kontaktu.
  AQQ_CONTACTS_ADD = 'AQQ/Contacts/Add';
  // Funkcja: TAK wParam=0 lParam=PPluginContact Res=0
  // Opis: Funkcja dodaj� nowy kontakt do listy kontakt�w.
  AQQ_CONTACTS_CREATE = 'AQQ/Contacts/Create';
  // Funkcja: TAK wParam=0 lParam=PPluginAddForm Res=0
  // Opis: Funkcja uruchamia okno dodawania nowego kontaktu. Mo�na wskaza� agenta kt�ry ma by� domy�lnie u�yty.
  AQQ_CONTACTS_ADDFORM = 'AQQ/Contacts/AddForm';
  // Notyfikacja: TAK wParam=0 lParam=PPluginContact Res=[0 nie dotyczy; 1 wtyczka zmieni�a przypisane grupy; 2 blokada]
  // Opis: Aktualizacja grup do kt�rych przynale�y kontakt.
  AQQ_CONTACTS_UPDATEGROUPS = 'AQQ/Contacts/UpdateGroups';
  // Notyfikacja: TAK wParam=0 lParam=PPluginContact Res=[0 nie dotyczy; 1 kontakt usuni�to; 2 blokada]
  // Opis: Informuje wtyczk� o usuni�ciu danego kontaktu.
  // Funkcja: TAK wParam=0 lParam=PPluginContact Res=0
  // Opis: Usuwa kontakt z listy kontakt�w.
  AQQ_CONTACTS_DELETE = 'AQQ/Contacts/Delete';
  // Funkcja: TAK wParam=0 lParam=PwideChar (domena np. @aqq.eu) Res=0
  // Opis: Usuwa z widocznej listy kontakt�w kontakty ze wskazanej domeny.
  AQQ_CONTACTS_DESTROYDOMAIN = 'AQQ/Contacts/DestroyDomain';
  // Notyfikacja: TAK wParam=PPluginContact lParam=Integer [sta�a typu zmiany statusu np. CONTACT_UPDATE_NORMAL ##015] Res=0
  // Opis: Informuje wtyczk� o aktualizacji kontaktu. Wtyczka mo�e zmodyfikowa� niekt�re pola PPluginContact (nickname, showtype, status).
  // Funkcja: TAK wParam=0 lParam=PPluginContact Res=0
  // Opis: Aktualizacja wskazanego kontaktu.
  AQQ_CONTACTS_UPDATE = 'AQQ/Contacts/Update';
  // Notyfikacja: TAK wParam=PPluginContact lParam=PWideChar [HTML kontaktu] Res=[0 bez zmian; 1 zasz�y zmiany]
  // Opis: Informuje wtyczk� tworzeniu wygl�du kontaktu na li�cie. Podana tre�� HTML mo�e zosta� zmodyfikowana.
  AQQ_CONTACTS_BUDDYFILL = 'AQQ/Contacts/BuddyFill';
  // Funkcja: TAK wParam=0 lParam=PPluginMessage Res=0
  // Opis: Wtyczka sieciowa mo�e poinformowa� o nowej wiadomo�c i przychodz�cej.
  AQQ_CONTACTS_MESSAGE = 'AQQ/Contacts/Message';
  // Notyfikacja: TAK wParam=PPluginContact lParam=PPluginMessage Res=[0 bez zmian; 1 blokada 2; modyfikacja]
  // Opis: Informuje wtyczk�, �e za moment zostanie wys�ana wskazana wiadomo�� tekstowa do kontaktu.
  AQQ_CONTACTS_PRESENDMSG = 'AQQ/Contacts/PreSendMsg';
  // Funkcja: TAK wParam=PPluginContact lParam=PPluginMessage Res=[0 domy�lnie; Integer ID wiadomo�ci > 100]
  // Opis: Wysy�a wiadomo�� tekstow� do wskazanego kontaktu.
  // Notyfikacja: TAK wParam=PPluginContact lParam=PPluginMessage Res=[0 nie dotyczy; 1 wiadomo�� wys�ana]
  // Opis: Informuj� o ch�ci wys�ania wiadomo�ci tekstowej. Odpowiedzialana za to wtyczka powinn� j� wys�a�.
  AQQ_CONTACTS_SENDMSG = 'AQQ/Contacts/SendMsg';
  // Notyfikacja: TAK wParam=PPluginContact lParam=PPluginMessage Res=[0 bez zmian; 1 blokada 2; modyfikacja]
  // Opis: Informuj� o otrzymaniu przez AQQ nowej wiadomo�ci tekstowej.
  AQQ_CONTACTS_RECVMSG = 'AQQ/Contacts/RecvMsg';
  // Funkcja: TAK wParam=Integer [AckId] lParam=Integer [sta�a typu stanu dor�czenia SMS ##020] Res=0
  // Opis: Stan wiadomo�ci o podanym ID zosta� zmieniony np. wiadomo�� zosta�a dor�czona
  AQQ_CONTACTS_ACKMSG = 'AQQ/Contacts/AckMsg';
  // Notyfikacja: TAK wParam=PPluginContact lParam=PPluginMessage Res=[0 bez zmian; 1 blokada 2; modyfikacja]
  // Opis: Informuj� o dodawaniu nowej linii do okna rozmowy (kod HTML). Wtyczka mo�e j� zmodyfikowa�.
  AQQ_CONTACTS_ADDLINE = 'AQQ/Contacts/AddLine';
  // Funkcja: TAK wParam=Integer (ID wywo�ania, prosz� generowa� przy pomocy systemowej funkcji WinAPI GetTickCount) lParam=0 Res=0
  // Opis: Funkcja wywo�uje enumeracje listy kontakt�w. Kolejne kontakty s� wysy�ane przy pomocy notyfikacji AQQ_CONTACTS_REPLYLIST.
  AQQ_CONTACTS_REQUESTLIST = 'AQQ/Contacts/RequestList';
  // Notyfikacja: TAK wParam=Integer (ID wywo�ania) lParam=PPluginContact Res=0
  // Opis: Notyfikacja wywo�ywana jest raz dla ka�dego kontaktu dost�pnego na li�cie. Gdy enumeracja dobiegnie ko�ca, AQQ poinfrmuje o tym za pomoc� notyfikacji AQQ_CONTACTS_REPLYLISTEND.
  AQQ_CONTACTS_REPLYLIST = 'AQQ/Contacts/ReplyList';
  // Notyfikacja: TAK wParam=Integer (ID wywo�ania) lParam=0 Res=0
  // Opis: Informuje, �e enumeracja kontakt�w (o wskazanym ID) dobieg�a ko�ca.
  AQQ_CONTACTS_REPLYLISTEND = 'AQQ/Contacts/ReplyListEnd';
  // Funkcja: TAK wParam=PPluginContact lParam=PWideChar (sformatowana tre�� HTML aktualnego opisu) Res=0
  // Opis: Pozwala zmieni� opis HTML na li�cie wskazanego kontaktu w dowolnym momencie.
  // Notyfikacja: TAK wParam=PPluginContact lParam=PWideChar (sformatowana tre�� HTML aktualnego opisu) Res=[0 bez zmian; PWideChar Zmodyfikowany status zgodny z HTML]
  // Opis: Notyfikacja pozwala podmieni� wygl�d statusu wskazanego kontaktu na li�cie. Modyfikowana jest tre�� HTML dlatego nale�y zachowa� szczeg�ln� ostro�no��.
  AQQ_CONTACTS_SETHTMLSTATUS = 'AQQ/Contacts/SetHTMLStatus';
  // Funkcja: TAK wParam=PPluginContact lParam=0) Res=[0 b��d; PWideChar sformatowany aktualny status HTML u�ytkownika na li�cie]
  // Opis: Funkcja pozwala pobra� wygl�d statusu wskazanego kontaktu na li�cie w formie HTML.
  AQQ_CONTACTS_GETHTMLSTATUS = 'AQQ/Contacts/GetHTMLStatus';
  // Funkcja: TAK wParam=PPluginContact lParam=PWideChar (�cie�ka do pliku) Res=[0 b��d; 1 ok]
  // Opis: Rozpoczyna wysy�k� obrazka do wskazanego kontaktu. Musi on istnie� w otwartym oknie rozmowy.
  // Notyfikacja: TAK wParam=PPluginMsgPic lParam=PPluginContact Res=[0 domy�lnie; -1 blokada ukrytych transfer�w]
  // Opis: Informuj� o rozpocz�ciu wysy�ki obrazka do wskazanego kontaktu.
  AQQ_CONTACTS_SENDPIC = 'AQQ/Contacts/SendPic';
  // Notyfikacja: TAK wParam=Integer (wielko�� pliku w bajtach) lParam=PPluginContact Res=[0 domy�lnie; -1 kontakt nie po��czony wysy�ka zabroniona; -2 kontakt po��czony ale nie obs�uguj� wysy�ka zabroniona; Integer max. wielko�� pliku w KB]
  // Opis: Informuj� o rozpocz�ciu wysy�ki obrazka do wskazanego kontaktu.
  AQQ_CONTACTS_SENDPIC_SIZECHECK = 'AQQ/Contacts/SendPic/SizeCheck';
  // Notyfikacja: TAK wParam=PPluginFileTransfer lParam=PPluginContact Res=[0 domy�lnie; Integer uchwyt wtyczki kt�ra odpowiada za transfer]
  // Opis: Informuj� o rozpocz�ciu wysy�ki pliku do wskazanego kontaktu.
  AQQ_CONTACTS_SENDFILE = 'AQQ/Contacts/SendFile';
  // Notyfikacja: TAK wParam=0 lParam=PPluginContact Res=[0 domy�lnie; PWideChar �cie�ka do pliku]
  // Opis: AQQ prosi o �cie�k� do pliku graficznego, kt�ry odzwierciedla aktualny stan wskazanego kontaktu.
  AQQ_CONTACTS_ICONSHOWTYPE_PATH = 'AQQ/Contacts/IconShowType/Path';
  // Notyfikacja: TAK wParam=0 lParam=PPluginContact Res=[0 domy�lnie; Integer index grafiki]
  // Opis: AQQ prosi o index grafiki, kt�ra odzwierciedla aktualny stan wskazanego kontaktu.
  AQQ_CONTACTS_ICONSHOWTYPE_INDEX = 'AQQ/Contacts/IconShowType/Index';
  // Notyfikacja: TAK wParam=0 lParam=PWideChar (JID) Res=[0 domy�lnie; Integer index grafiki]
  // Opis: AQQ prosi o index grafiki, kt�ra odzwierciedla aktualny stan wskazanego kontaktu.
  AQQ_CONTACTS_ICONSHOWTYPEJID_INDEX = 'AQQ/Contacts/IconShowTypeJID/Index';
  // Notyfikacja: TAK wParam=0 lParam=PPluginContact Res=[0 domy�lnie; Integer index grafiki HD]
  // Opis: AQQ prosi o index grafiki HD, kt�ra odzwierciedla aktualny stan wskazanego kontaktu.
  AQQ_CONTACTS_ICONSHOWTYPE_HDINDEX = 'AQQ/Contacts/IconShowType/HDIndex';
  // Funkcja: TAK wParam=0 lParam=PWideChar (domena np. @aqq.eu) Res=0
  // Opis: Ustawia kontakty we wskazanej domenie w trybie offline (roz��czone).
  AQQ_CONTACTS_SETOFFLINE = 'AQQ/Contacts/SetOffline';
  // Notyfikacja: TAK wParam=0 lParam=PPluginContact Res=0
  // Opis: AQQ prosi o przestawienie wskazanych kontakt�w w tryb offline (roz��czony).
  AQQ_CONTACTS_OFFLINE = 'AQQ/Contacts/Offline';
  // Notyfikacja: TAK wParam=0 lParam=PWideChar (JID agenta) Res=[0 nie dotyczy; XML]
  // Opis: AQQ prosi o podanie przez wtyczk� sieciow�, opisu wyszukiwarki wed�ug standardu http://xmpp.org/extensions/xep-0055.html
  AQQ_CONTACTS_GETSEARCHXML = 'AQQ/Contacts/GetSearchXML';
  // Notyfikacja: TAK wParam=PWideChar (wype�niony XML) lParam=PWideChar (JID agenta) Res=[0 nie dotyczy; 1 ok]
  // Opis: U�ytkownik szuka znajomych a AQQ zwraca do wtyczki wype�niony XML wed�ug standardu XEP-0055 - dla ponadego agenta.
  AQQ_CONTACTS_SETSEARCHXML = 'AQQ/Contacts/SetSearchXML';
  // Funkcja: TAK wParam=Integer (ID sesji wyszukiwania) lParam=PwideChar (xml) Res=0
  // Opis: Wtyczka sieciowa zwraca wyniki wyszukiwania kontakt�w (wed�ug standardu XEP-0055).
  AQQ_CONTACTS_RESSEARCHXML = 'AQQ/Contacts/ResSearchXML';
  // Funkcja: TAK wParam=PwideChar (xml) lParam=0 Res=0
  // Opis: Wtyczka sieciowa zwraca b��d wynik�y w trakcie wyszukiwania kontakt�w (wed�ug standardu XEP-0055).
  AQQ_CONTACTS_ERRSEARCHXML = 'AQQ/Contacts/ErrSearchXML';
  // Notyfikacja: TAK wParam=0 lParam=PWideChar ID Res=0
  // Opis: Przed rozpocz�ciem wyszukiwania, AQQ nadaje mu nowe ID. I informuje o nim wtyczk� sieciow�.
  AQQ_CONTACTS_LASTSEARCHID = 'AQQ/Contacts/LastSearchID';
  // Notyfikacja: TAK wParam=PWideChar (nowy pseudonim) lParam=PPluginContact ID Res=0
  // Opis: AQQ informuje o zmianie pseudonimu kontaktu. Wtyczka do kt�rej przynale�y kontakt powinna uaktualni� pseudonim.
  AQQ_CONTACTS_CHANGENAME = 'AQQ/Contacts/ChangeName';
  // Notyfikacja: TAK wParam=0 lParam=PPluginTwoFlagParams (Param1 nazwa grupy; Param2 nowa nazwa; Flag1 ID konta) Res=0
  // Opis: AQQ informuje o zmianie nazwy grupy. Wtyczka do kt�rej przynale�y grupa powinna si� dostosowa� do zmian.
  AQQ_CONTACTS_CHANGEGROUPNAME = 'AQQ/Contacts/ChangeGroupName';
  // Notyfikacja: TAK wParam=PWideChar (ID) lParam=PPluginContact Res=[0 nie dotyczy; 1 ok]
  // Opis: AQQ prosi o pobranie wizyt�wki kontaktu z serwera, przy zwrocie danych nale�y uwzgl�dni� ID podane przez AQQ.
  AQQ_CONTACTS_GETVCARD = 'AQQ/Contacts/GetVCard';
  // Funkcja: TAK wParam=PwideChar (ID) lParam=PWideChar (xml) Res=0
  // Opis: Wtyczka sieciowa zwraca wizyt�wk� kontaktu w standardzie XEP-0054: vcard-temp
  AQQ_CONTACTS_RESVCARD = 'AQQ/Contacts/ResVCard';
  // Notyfikacja: TAK wParam=0 lParam=PPluginContact Res=0
  // Opis: Informacja o aktualnie zaznaczonym kontakcie na li�cie.
  AQQ_CONTACTS_BUDDY_SELECTED = 'AQQ/Contacts/Buddy/Selected';
  // Notyfikacja: TAK wParam=Integer (uchwyt okna) lParam=PPluginContact Res=0
  // Opis: Informacja o aktualnie wybranej zak�adce w oknie rozmowy.
  AQQ_CONTACTS_BUDDY_ACTIVETAB = 'AQQ/Contacts/Buddy/ActiveTab';
  // Notyfikacja: TAK wParam=Integer (uchwyt okna) lParam=PPluginContact Res=0
  // Opis: Informacja o aktualnie zamykanej zak�adce w oknie rozmowy.
  AQQ_CONTACTS_BUDDY_CLOSETAB = 'AQQ/Contacts/Buddy/CloseTab';
  // Funkcja: TAK wParam=[0 domy�lnie; 1 kontakt pochodzi z wtyczki] lParam=PWideChar (JID kontaktu) Res=0
  // Opis: Zamyka wskazan� zak�adk� z kontaktem w oknie rozmowy.
  // Notyfikacja: TAK wParam=PWideChar (wiadomo��) lParam=PPluginContact Res=0
  // Opis: Informacja o aktualnie zamykanej zak�adce w oknie rozmowy - i o nie wys�anej wiadomo�ci kt�rej u�ytkownik nie wys�a�. Je�eli wiadomo�� jest pusta - notyfikacja nie wyst�pi.
  AQQ_CONTACTS_BUDDY_CLOSETABMESSAGE = 'AQQ/Contacts/Buddy/CloseTabMessage';
  // Notyfikacja: TAK wParam=Integer (uchwyt okna) lParam=PPluginContact Res=0
  // Opis: Informacja o aktualnie aktywnym oknie, i jaki kontakt w tym oknie jest aktywny obecnie.
  AQQ_CONTACTS_BUDDY_FORMACTIVATE = 'AQQ/Contacts/Buddy/FormActive';
  // Funkcja: TAK wParam=0 lParam=0 Res=0
  // Opis: Rozpoczyna wysy�anie notyfikacji na temat otwartych zak�adek w oknie rozmowy.
  // Notyfikacja: TAK wParam=Integer (uchwyt okna) lParam=PPluginContact Res=0
  // Opis: Informuje, �e taka zak�adka w oknie rozmowy jest otwarta. Wymaga wcze�niejszego u�ycia funkcji AQQ_CONTACTS_BUDDY_FETCHALLTABS.
  AQQ_CONTACTS_BUDDY_FETCHALLTABS = 'AQQ/Contacts/Buddy/FetchAllTabs';
  // Notyfikacja: TAK wParam=Integer (uchwyt okna) lParam=PPluginContact Res=0
  // Opis: Informuje, �e taka zak�adka w oknie rozmowy jest otwarta i jest aktywna. Wymaga wcze�niejszego u�ycia funkcji AQQ_CONTACTS_BUDDY_FETCHALLTABS.
  AQQ_CONTACTS_BUDDY_PRIMARYTAB = 'AQQ/Contacts/Buddy/PrimaryTab';
  // Notyfikacja: TAK wParam=Integer (uchwyt okna) lParam=PPluginContact Res=0
  // Opis: Informuje, �e taka zak�adka w oknie rozmowy jest otwarta, i jest to czat. Wymaga wcze�niejszego u�ycia funkcji AQQ_CONTACTS_BUDDY_FETCHALLTABS.
  AQQ_CONTACTS_BUDDY_CONFERENCETAB = 'AQQ/Contacts/Buddy/ConferenceTab';
  // Funkcja: TAK wParam=PWideChar (nowa etykieta zak�adki) lParam=PPluginContact Res=0
  // Opis: Zmienia etykiete zak�adki w otwartym oknie rozmowy.
  // Notyfikacja: TAK wParam=PWideChar (nowa etykieta zak�adki) lParam=PPluginContact Res=[0 ok; PWideChar zmiana nazwy]
  // Opis: Informuje o zmianie etykiety zak�adki w otwartym oknie rozmowy.
  AQQ_CONTACTS_BUDDY_TABCAPTION = 'AQQ/Contacts/Buddy/TabCaption';
  // Funkcja: TAK wParam=Integer (indeks nowej ikony) lParam=PPluginContact Res=0
  // Opis: Zmienia ikonk� zak�adki w otwartym oknie rozmowy.
  // Notyfikacja: TAK wParam=Integer (nowy indeks ikony) lParam=PPluginContact Res=[0 ok; Integer nowa indeks ikony]
  // Opis: Informuje o zmianie ikony zak�adki w otwartym oknie rozmowy.
  AQQ_CONTACTS_BUDDY_TABIMAGE = 'AQQ/Contacts/Buddy/TabImage';
  // Funkcja: TAK wParam=PWideChar (URL) lParam=PPluginContact Res=0
  // Opis: Pobiera awatara z podanego adres URL, i ustawia go dla wskazanego kontaktu.
  AQQ_CONTACTS_SETWEB_AVATAR = 'AQQ/Contacts/SetWeb/Avatar';
  // Funkcja: TAK wParam=PPluginAvatar lParam=PPluginContact Res=0
  // Opis: Ustawia nowy awatar dla wskazanego kontaktu.
  AQQ_CONTACTS_SET_AVATAR = 'AQQ/Contacts/Set/Avatar';
  // Notyfikacja: TAK wParam=PPluginContact lParam=0 Res=[0 domy�lnie; PWideChar stanu kontaktu w formie tekstowej]
  // Opis: AQQ prosi o podanie stanu kontaktu w formie tekstowej dla podanego kontaktu (np. Po��czony).
  AQQ_CONTACTS_STATUSCAPTION = 'AQQ/Contacts/StatusCaption';
  // Notyfikacja: TAK wParam=PPluginContact lParam=1 Res=0
  // Opis: AQQ prosi o zresetowanie awatara dla wskazanego kontaktu (nale�y go pobra� od nowa).
  AQQ_CONTACTS_RESETAVATAR = 'AQQ/Contacts/ResetAvatar';
  // Notyfikacja: TAK wParam=PPluginContact lParam=PPluginMessage Res=[0 domy�lnie; 1 blokada lub w�asna obs�uga zdarzenia]
  // Opis: Wskazany kontakt prosi u�ytkownika o uwag�.
  AQQ_CONTACTS_ATTENTION = 'AQQ/Contacts/Attention';
  // Notyfikacja: TAK wParam=0 lParam=PWideChar (jid) Res=[0 domy�lnie; 1 blokada lub w�asna obs�uga zdarzenia]
  // Opis: AQQ pyta wtyczk� do kt�rej przynale�y kontakt, czy podany JID jest zgodny ze standardami sieci kt�r� ta wtyczka obs�uguj� (np. same liczby)
  AQQ_CONTACTS_VALIDATEJID = 'AQQ/Contacts/ValidateJID';

  /// Ikony
  // Funkcja: TAK wParam=0 lParam=PWideChar (�cie�ka do ikony) Res=[-1 b��d; Integer index dodanej ikony]
  // Opis: Dodaje now� ikon� do zasob�w AQQ. Ikona powinna by� 32 bitowa + kana� alpha i w rozmiarach 16x16.
  AQQ_ICONS_LOADPNGICON = 'AQQ/Icons/LoadPNGIcon';
  // Funkcja: TAK wParam=Integer index ikony do zmiany lParam=PWideChar (�cie�ka do ikony) Res=[-1 b��d; Integer index zmienionej ikony]
  // Opis: Dodaje now� ikon� do zasob�w AQQ. Ikona powinna by� 32 bitowa + kana� alpha i w rozmiarach 16x16.
  AQQ_ICONS_REPLACEPNGICON = 'AQQ/Icons/ReplacePNGIcon';
  // Funkcja: TAK wParam=0 lParam=Integer (index ikony do usuni�cia) Res=0
  // Opis: Usuwa ikon� o podanym indexie z zasob�w AQQ. Wtyczka powinna usuwa� indexy kt�re sama doda�a wcze�niej.
  AQQ_ICONS_DESTROYPNGICON = 'AQQ/Icons/DestroyPNGIcon';

  //// Funkcje
  // Funkcja: TAK wParam=0 lParam=0 Res=[0 nie; 1 tak]
  // Opis: Stwierdza, czy lista kontakt�w jest gotowa na wykonywanie jakichkolwiek operacji np. dodawania kontakt�w.
  AQQ_FUNCTION_ISLISTREADY = 'AQQ/Function/IsListReady';
  // Funkcja: TAK wParam=Integer (uchwyt wtyczki) lParam=0 Res=[0; b��d PWideChar �cie�ka do katalogu wtyczki]
  // Opis: Zwraca katalog w kt�rym znajduj� si� wtyczka o podanym uchwycie (hInstance).
  AQQ_FUNCTION_GETPLUGINDIR = 'AQQ/Function/GetPluginDir';
  // Funkcja: TAK wParam=0 lParam=0 Res=PWideChar (�cie�ka)
  // Opis: Zwraca �cie�k� do profilu zalogowanego w danej chwili u�ytkownika.
  AQQ_FUNCTION_GETUSERDIR = 'AQQ/Function/GetUserDir';
  // Funkcja: TAK wParam=0 lParam=0 Res=PWideChar (�cie�ka)
  // Opis: Zwraca �cie�k� do katalogu zawieraj�cego wtyczki przypisane do zalogowanego w danej chwili u�ytkownika.
  AQQ_FUNCTION_GETPLUGINUSERDIR = 'AQQ/Function/GetPluginUserDir';
  // Funkcja: TAK wParam=0 lParam=PWideChar (sta�a tekstowa) Res=PWideChar (tekst)
  // Opis: Podaj�c sta�a tekstow� wybran� z pliku Const.lng, otrzymamy jej warto�c tekstow� - u�yty b�dzie j�zyk z kt�rego korzysta zalogowany u�ytkownik.
  AQQ_FUNCTION_GETLANGSTR = 'AQQ/Function/GetLangStr';
  // Funkcja: TAK wParam=PWideChar (adres url) lParam=0 Res=PWideChar (token)
  // Opis: Funkcja zwraca przepisany przez u�ytkownika Token, pochodz�cy ze strony webowej wskazanej w parametrze wParam.
  AQQ_FUNCTION_GETTOKEN = 'AQQ/Function/GetToken';
  // Funkcja: TAK wParam=0 lParam=0 Res=PWideChar (ID)
  // Opis: Funkcja zwraca unikalne (na czas trwania sesji AQQ) ID tekstowe.
  AQQ_FUNCTION_GETSTRID = 'AQQ/Function/GetStrID';
  // Funkcja: TAK wParam=0 lParam=0 Res=Integer (ID)
  // Opis: Funkcja zwraca unikalne (na czas trwania sesji AQQ) ID numeryczne.
  AQQ_FUNCTION_GETNUMID = 'AQQ/Function/GetNumID';
  // Funkcja: TAK wParam=0 lParam=0 Res=PWideChar (JID/UID)
  // Opis: Funkcja zwraca JID/UID zalogowanego w AQQ u�ytkownika (g��wne konto).
  AQQ_FUNCTION_GETUSERUID = 'AQQ/Function/GetUserUID';
  // Funkcja: TAK wParam=0 lParam=0 Res=PWideChar (�cie�ka)
  // Opis: Funkcja zwraca �cie�k� do katalogu kompozycji w kt�rej wtyczka b�dzie mia�a prawa zapisu. Je�eli taki katalog jest nie dost�pny - zwr�cony zostanie pusty ci�g znak�w!
  AQQ_FUNCTION_GETTHEMEDIRRW = 'AQQ/Function/GetThemeDirRW';
  // Funkcja: TAK wParam=0 lParam=0 Res=PWideChar (�cie�ka)
  // Opis: Funkcja zwraca �cie�k� do katalogu u�ywanej aktualnie kompozycji wizualnej.
  AQQ_FUNCTION_GETTHEMEDIR = 'AQQ/Function/GetThemeDir';
  // Funkcja: TAK wParam=0 lParam=0 Res=PPluginProxy
  // Opis: Funkcja zwraca aktualne ustawienia Proxy w AQQ.
  AQQ_FUNCTION_GETPROXY = 'AQQ/Function/GetProxy';
  // Funkcja: TAK wParam=0 lParam=0 Res=PWideChar (�cie�ka)
  // Opis: Funkcja zwraca �cie�k� do katalogu instalacyjnego AQQ.
  AQQ_FUNCTION_GETAPPPATH = 'AQQ/Function/GetAppPath';
  // Funkcja: TAK wParam=0 lParam=0 Res=PWideChar (�cie�ka)
  // Opis: Funkcja zwraca �cie�k� do pliku wykonywalnego AQQ (plik kt�ry zosta� u�yty do rozruchu aplikacji).
  AQQ_FUNCTION_GETAPPFILEPATH = 'AQQ/Function/GetAppFilePath';
  // Funkcja: TAK wParam=[0 domy�lnie; 1 kontakt pochodzi z wtyczki] lParam=PWideChar (JID) Res=0
  // Opis: Funkcja otwiera okno rozmowy ze wskazanym kontaktem. Okno rozmowy zostaje przywo�ane na pierwszy plan.
  AQQ_FUNCTION_EXECUTEMSG = 'AQQ/Function/ExecuteMsg';
  // Funkcja: TAK wParam=[0 domy�lnie; 1 kontakt pochodzi z wtyczki; 2 kontakt to konferencja] lParam=PWideChar (JID) Res=0
  // Opis: Funkcja otwiera okno rozmowy ze wskazanym kontaktem. Stan okna rozmowy nie zmienia si� (brak przywo�ania na pierwszy plan).
  AQQ_FUNCTION_EXECUTEMSG_NOPRIORITY = 'AQQ/Function/ExecuteMsg/NoPriority';
  // Funkcja: TAK wParam=[0 domy�lnie; 1 kontakt pochodzi z wtyczki] lParam=PWideChar (JID) Res=0
  // Opis: Funkcja dodaje kontakt do listy zamkni�tych ostatnio zak�adek. Lista ta wykorzystywana jest m.inn. w dymku ikony AQQ w zasobniku systemowym.
  AQQ_FUNCTION_TABWASCLOSED = 'AQQ/Function/TabWasClosed';
  // Funkcja: TAK wParam=PPluginTriple (Handle1 uchwyt okna; Param1 index zak�adki do zmiany; Param2 nowy index) lParam=0 Res=[0 b��d; 1 ok]
  // Opis: Funkcja zamienia miejscami dwie wybrane zak�adki.
  AQQ_FUNCTION_TABMOVE = 'AQQ/Function/TabMove';
  // Funkcja: TAK wParam=PPluginTriple (Handle1 uchwyt okna) lParam=0 Res=[0 b��d; Integer ilo�� otwartych zak�adek]
  // Opis: Funkcja zwraca ilo�� otwartych zak�adek we wskazanym oknie rozmowy.
  AQQ_FUNCTION_TABCOUNT = 'AQQ/Function/TabCount';
  // Funkcja: TAK wParam=[0 domy�lnie; 1 kontakt pochodzi z wtyczki] lParam=PWideChar (JID) Res=[-1 b��d; Integer index]
  // Opis: Funkcja zwraca index zak�adki dla wskazanego kontaktu.
  AQQ_FUNCTION_TABINDEX = 'AQQ/Function/TabIndex';
  // Funkcja: TAK wParam=0 lParam=PWideChar (adres url) Res=0
  // Opis: Funkcja otwiera domy�ln� przegl�dark� internetow�, i przechodzi pod adres wskazany przez wtyczk�.
  AQQ_FUNCTION_OPENURL = 'AQQ/Function/OpenURL';
  // Funkcja: TAK wParam=0 lParam=PWideChar (adres url do filmu YT) Res=0
  // Opis: Funkcja otwiera odtwarzacz film�w YT i odtwarza wskazany materia� filmowy.
  AQQ_FUNCTION_OPENYTURL = 'AQQ/Function/OpenYTURL';
  // Funkcja: TAK wParam=0 lParam=0 Res=PWideChar (adres zew. IP)
  // Opis: Funkcja zwraca zewn�trzny adres IP u�ytkownika. Aby zosta� zwr�cony, AQQ musi skutecznie pobra� te dane przy starcie aplikacji.
  AQQ_FUNCTION_GETEXTERNALIP = 'AQQ/Function/GetExternalIP';
  // Funkcja: TAK wParam=0 lParam=0 Res=Integer (liczba kont)
  // Opis: Funkcja zwraca liczb� kont aktywnych na zalogowanym profilu u�ytkownika.
  AQQ_FUNCTION_GETUSEREXCOUNT = 'AQQ/Function/GetUserExCount';
  // Funkcja: TAK wParam=0 lParam=Integer (ID konta) Res=PPluginStateChange
  // Opis: Funkcja zast�piona przez AQQ_FUNCTION_GETNETWORKSTATE. Nie stosowa�. Zwraca informacje na temat stanu wskazanego konta u�ytkownika.
  AQQ_FUNCTION_GETNETWORKINFO = 'AQQ/Function/GetNetworkInfo';
  // Funkcja: TAK wParam=PPluginStateChange (wyzerowana struktura) lParam=Integer (ID konta) Res=[0 b��d; 1 konto; 2; konto pochodz�ce z wtyczki]
  // Opis: Zwraca informacje na temat stanu wskazanego konta u�ytkownika. AQQ wype�nia struktur� podan� przez wtyczk� w parametrze wParam.
  AQQ_FUNCTION_GETNETWORKSTATE = 'AQQ/Function/GetNetworkState';
  // Funkcja: TAK wParam=0 lParam=PWideChar (tekst) Res=PWideChar (tekst zgodny z XML)
  // Opis: Funkcja zmienia znaki specjalne na zgodne ze specyfik� standardu XML.
  AQQ_FUNCTION_CONVERTTOXML = 'AQQ/Function/ConvertToXML';
  // Funkcja: TAK wParam=0 lParam=PPluginContact Res=Integer (index ikony)
  // Opis: Funkcja zwraca index ikony stanu, kt�ry jest przypisany aktualnie do wskazanego kontaktu.
  AQQ_FUNCTION_GETSTATEPNG_INDEX = 'AQQ/Function/GetStatePNG/Index';
  // Funkcja: TAK wParam=0 lParam=PPluginContact Res=PWideChar (�cie�ka do ikony)
  // Opis: Funkcja zwraca �cie�k� do ikony stanu, kt�ra jest przypisana aktualnie do wskazanego kontaktu.
  AQQ_FUNCTION_GETSTATEPNG_FILEPATH = 'AQQ/Function/GetStatePNG/FilePath';
  // Funkcja: TAK wParam=Integer (index ikony) lParam=0 Res=PWideChar (�cie�ka do ikony)
  // Opis: Funkcja zwraca �cie�k� do ikony o indexie podanym w parametrze wParam.
  AQQ_FUNCTION_GETPNP_FILEPATH = 'AQQ/Function/GetPNG/FilePath';
  // Funkcja: TAK wParam=0 lParam=Integer (index ikony) Res=PWideChar (�cie�ka do ikony HD)
  // Opis: Funkcja zwraca �cie�k� do ikony HD o indexie podanym w parametrze lParam.
  AQQ_FUNCTION_GETPNGHD_FILEPATH = 'AQQ/Function/GetPNGHD/FilePath';
  // Funkcja: TAK wParam=Integer (typ wiadomo�ci: 0 informacja; 1 ostrze�enie; 2 pytanie ##010) lParam=PWideChar (tekst) Res=[1 ok; 2 analuj; 3 zaniechaj; 4 pr�buj ponownie; 5 ignoruj; 6 tak; 7 nie; 8 zamknij; 9 pomoc]
  // Opis: Funkcja wy�wietla okno dialogowe o wskazanym typie, i zwraca wynik akcji u�ytkownika wobec informacji zawartej w parametrze lParam.
  AQQ_FUNCTION_SHOWMESSAGE = 'AQQ/Function/ShowMessage';
  // Funkcja: TAK wParam=0 lParam=Integer (sta�a stanu np. CONTACT_OFFLINE ##016) Res=PWideChar (tekstowa reprezentacja stanu)
  // Opis: Funkcja zmienia sta�� stanu, na tekst - np. "Roz��czony".
  AQQ_FUNCTION_STATETOSTR = 'AQQ/Function/StateToStr';
  // Funkcja: TAK wParam=[0 domy�lnie; 1 dodaj� tekst do pliku loga pomimo wy��czonego logowania] lParam=PWideChar (tekst) Res=0
  // Opis: Funkcja dodaj� wskazany tekst do pliku log-a.
  AQQ_FUNCTION_LOG = 'AQQ/Function/Log';
  // Funkcja: TAK wParam=0 lParam=PPluginShowInfo Res=0
  // Opis: Funkcja wy�wietla chmurk� informacyjn� skonstruowan� przy pomocy wype�nionej struktury PPluginShowInfo.
  // Notyfikacja: TAK wParam=0 lParam=PPluginShowInfo Res=[0 ok; 1 blokada wy�wietlenia]
  // Opis: Informuje o pr�bie wy�wietlenia chmurki informacyjnej. Mo�na j� zablokowa� zwracaj�c warto�� 1.
  AQQ_FUNCTION_SHOWINFO = 'AQQ/Function/ShowInfo';
  // Funkcja: TAK wParam=0 lParam=0 Res=Integer [kolejny numer startu AQQ]
  // Opis: Funkcja zwraca liczb� start�w AQQ na danym profilu.
  AQQ_FUNCTION_GETSTARTCOUNT = 'AQQ/Function/GetStartCount';
  // Funkcja: TAK wParam=0 lParam=0 Res=PWideChar [plik INI]
  // Opis: Funkcja zwraca ustawienia AQQ zgodnie ze standardem pliku INI.
  AQQ_FUNCTION_FETCHSETUP = 'AQQ/Function/FetchSetup';
  // Funkcja: TAK wParam=[0 domy�lnie; 1 od�wie�a ustawienia w AQQ] lParam=PSaveSetup Res=0
  // Opis: Funkcja zapisuje nowy element ustawie� AQQ.
  AQQ_FUNCTION_SAVESETUP = 'AQQ/Function/SaveSetup';
  // Funkcja: TAK wParam=0 lParam=0 Res=0
  // Opis: Funkcja pobiera na nowo dane z pliku konfiguracyjnego AQQ. Nast�puje od�wie�enie.
  AQQ_FUNCTION_REFRESHSETUP = 'AQQ/Function/RefreshSetup';
  // Funkcja: TAK wParam=0 lParam=[0 domy�lnie; 1 wymusza sprawdzenie; 2; wymusza i sprawdza te� dodatki] Res=0
  // Opis: Funkcja sprawdza dost�pne aktualizacje.
  AQQ_FUNCTION_SILENTUPDATECHECK = 'AQQ/Function/SilentUpdateCheck';
  // Funkcja: TAK wParam=0 lParam=0 Res=Integer (uchwyt do pliku DLL)
  // Opis: Funkcja zwraca uchwyt za�adowanej biblioteki OpenSSL: libssl32.dll - wtyczka nie musi �adowa� jej sama ponownie.
  AQQ_FUNCTION_SSLHANDLE = 'AQQ/Function/SSLHandle';
  // Funkcja: TAK wParam=0 lParam=0 Res=Integer (uchwyt do pliku DLL)
  // Opis: Funkcja zwraca uchwyt za�adowanej biblioteki OpenSSL: libeay32.dll - wtyczka nie musi �adowa� jej sama ponownie.
  AQQ_FUNCTION_SSLLIBHANDLE = 'AQQ/Function/SSLLibHandle';
  // Funkcja: TAK wParam=PWideChar (jid) lParam=Integer (ID konta) Res=0
  // Opis: Funkcja wczytuje ostatni� rozmow� do otwartego ju� okna rozmowy ze wskazanym kontaktem.
  AQQ_FUNCTION_LOADLASTCONV = 'AQQ/Function/LoadLastConv';
  // Funkcja: TAK wParam=0 lParam=0 Res=0
  // Opis: Funkcja wywo�uj� funkcje Application.ProcessMessages po stronie AQQ.
  AQQ_FUNCTION_PROCESSMESSAGES = 'AQQ/Function/ProcessMessages';
  // Funkcja: TAK wParam=0 lParam=PWideChar (tekst) Res=PWideChar (znormalizowany tekst)
  // Opis: Funkcja normalizuje nazwy pokoi czatowych.
  AQQ_FUNCTION_NORMALIZE = 'AQQ/Function/Normalize';

/// !Consts - Sta�e

const //##001 // Account Events - sta�e akcji kont
  ACCOUNT_EVENT_DEFAULT = 0; // Domy�lne zdarzenie przy edycji kont
  ACCOUNT_EVENT_NEW = 1; // Nowe konto
  ACCOUNT_EVENT_EDIT = 2; // Edycja konta
  ACCOUNT_EVENT_DELETE = 3; // Usuni�cie konta
  ACCOUNT_EVENT_CHANGEPASS = 4; // Zmiana has�a dla konta

const //##002 // System Function - sta�e funkcji systemowych
  SYS_FUNCTION_SEARCHONLIST = 1; // Wy��czenie funkcji szukania na li�cie kontakt�w
  SYS_FUNCTION_ANTISPIM_LEN = 2; // Wy��czenie funkcji ochrony anty-spimowej
  SYS_FUNCTION_TASKBARPEN = 3; // Wy��czenie funkcji pisaka przy zminimalizowanym oknie rozmowy
  SYS_FUNCTION_CLOSEBTN = 4; // Wy��czenie przycisku "x" na zak�adkach w oknie rozmowy
  SYS_FUNCTION_MSGCOUNTER = 5; // Wy��czenie graficznego licznika nieprzeczytanych wiadomo�ci na zak�adkach w oknie rozmowy

const //##003 // Tabs - sta�e zak�adek
  TAB_JABBER = 1; // Zak�adka z list� kontakt�w
  TAB_SMS = 2; // Zak�adka SMS
  TAB_MULTICHAT = 3; // Zak�adka czat�w
  TAB_NEWS = 4; // Zak�adka powiadomie�

const //##004
  SETNOTE_ABORT = 1; // wtyczka nie powinna zmienia� opisu, je�eli to ona wywo�a�a okno zmiany opisu.

const //##005
  CHATMODE_NORMAL = 0; // Tryb czatu normalny (z podzia�em na grupy)
  CHATMODE_SIMPLE = 1; // Tryb czatu bez podzia�u na grupy

const //##006
  ROLE_VISITOR = 'visitor'; // Go��
  ROLE_MODERATOR = 'moderator'; // Moderator
  ROLE_PARTICIPANT = 'participant'; // Uczestnik
  ROLE_OBSERVER = 'none'; // Brak

const //##007
  AFFILIATION_ADMIN = 'admin'; // Administrator pokoju
  AFFILIATION_OUTCAST = 'outcast'; // Zbanowany
  AFFILIATION_MEMBER = 'member'; // Uczestnik
  AFFILIATION_OWNER = 'owner'; // W�a�ciciel
  AFFILIATION_NONE = 'none'; // Brak

const //##008 // sta�a po��cze�
  CONNSTATE_DISCONNECTED = 1; // Roz��czono
  CONNSTATE_CONNECTING = 2; // Trwa ��czenie z sieci�
  CONNSTATE_CONNECTED = 3; // Po��czono

const //##009
  TOOLTIP_EVENT_TITLE = 0; // Tuty�
  TOOLTIP_EVENT_DATA = 1; // Data
  TOOLTIP_EVENT_STATUS = 2; // Status tekstowy
  TOOLTIP_EVENT_AUTH = 3; // Stan subskrypcji
  TOOLTIP_EVENT_ACTIVITY = 4; // Ostatnia aktywno��

const //##010
  SM_INFO = 0; // Informacja
  SM_WARN = 1; // Ostrze�enie
  SM_QUESTION = 2; // Pytanie

const //##011 // sta�e zmiany statusu w oknie
  WINDOW_STATUS_NONE = 0; // �adna wtyczka nie zainteresowana.
  WINDOW_STATUS_SET = 1; // Wtyczka zainteresowana i ustawi�a status.
  WINDOW_STATUS_SETEXIT = 2; // Wtyczka zainteresowana, ona wywo�a�a okno i AQQ powinno je tylko zamkn�� (brak zmian na kontach Jabber).

const //##012
  AQQ_CACHE_ITEM = 'AQQ_CACHE_ITEM'; // U�ywane w tre�ci wiadomo�ci jako sta�a obrazka.
  AQQ_CACHE_GHOSTITEM = 'AQQ_CACHE_GHOSTITEM'; // Nie wykorzystywane przez wtyczki aktualnie.

const //##013 // sta�e sprawdzaj�ce
  AQQ_QUERY_DELETE = 1; // Mo�na usun�� kontakt/grup�
  AQQ_QUERY_MESSAGE = 2; // Mo�na wys�a� wiadomo��
  AQQ_QUERY_SENDFILE = 3; // Mo�na wys�aa� plik
  AQQ_QUERY_AUTHREQUEST = 4; // Mo�na wys�a� pro�b� o subskrypcj�
  AQQ_QUERY_SENDPIC = 5; // Mo�na wys�a� obrazek
  AQQ_QUERY_AUTH = 6; // Mo�na ustawia� subskrypcje
  AQQ_QUERY_CANCELAUTH = 7; // Mo�na wycofa� subskrypcje
  AQQ_QUERY_SMS = 8; // Mo�na wysy�a� SMS
  AQQ_QUERY_VCARD = 9; // Mo�na edytowa� wizyt�wk�
  AQQ_QUERY_ARCHIVE = 10; // Mo�na korzysta� z archiwum wiadomo�ci
  AQQ_QUERY_NETWORK = 11; // Zast�pione przez AQQ_QUERY_NETWORKSTATE
  AQQ_QUERY_HAVEVCARD = 12; // Kontakt posiada wizyt�k�
  AQQ_QUERY_ATTENTION = 13; // Mo�na prosi� o uwag�
  AQQ_QUERY_NETWORKSTATE = 14; // Stan po��czenia

const //##014 // sta�e sprawdzaj�ce -> Stany i opisy
  AQQ_QUERY_STATE = 15; // Obs�uga stan�w
  AQQ_QUERY_NOTE = 16; // Obs�uga opis�w
  AQQ_QUERY_ONLINE = 17; // Obs�uga stanu po��czonego
  AQQ_QUERY_FFC = 18; // Obs�uga stanu - ch�tny do rozmowy
  AQQ_QUERY_AWAY = 19; // Obs�uga stanu oddalonego
  AQQ_QUERY_XA = 20; // Obs�uga stanu nieobecnego
  AQQ_QUERY_DND = 21; // Obs�uga stanu nie przeszkadza�
  AQQ_QUERY_OFFLINE = 22; // Obs�uga stanu roz��czonego

const //##015 // sta�e typu zmiany statusu
  CONTACT_UPDATE_OFFLINE = -1; // Kontakt zmieni� stan na roz��czony
  CONTACT_UPDATE_NORMAL = 0; // Kontakt nale�y zaktualizowa� w normalnym trybie
  CONTACT_UPDATE_NOOFFLINE = 1; // Kontakt zmieni� stan na inny ni� roz��czony
  CONTACT_UPDATE_ONLINE = 2; // Kontakt w�a�nie si� po��czy�
  CONTACT_UPDATE_ONLYSTATUS = 3; // Zaktualizowa� nale�y jedynie stan i status kontaktu

const //##016 // sta�e stanu
  CONTACT_OFFLINE = 0; // Kontakt roz��czony
  CONTACT_ONLINE = 1; // Kontakt po��czony
  CONTACT_FFC = 2; // Kontakt ch�tny do rozmowy
  CONTACT_AWAY = 3; // Kontakt oddalony
  CONTACT_NA = 4; // Kontakt nieobecny
  CONTACT_DND = 5; // Nie przeszkadza�
  CONTACT_INV = 6; // Kontakt niewidoczny
  CONTACT_NULL = 7; // Nie okre�lony stan / zablokowany

const //##017
  DEBUG_XMLIN = 0; // Pakiet XML przychodz�cy
  DEBUG_XMLOUT = 1; // Pakiet XML wychodz�cy

const //##018 // sta�e online
  ONCHECK_COPYTO = 1; // Wtyczka zezwala na kopiowanie kontakt�w mi�dzy grupami
  ONCHECK_DELETE = 2; // Wtyczka pozwala na usuwanie kontakt�w
  ONCHECK_MOVETO = 3; // Wtyczka pozwala na przesuwanie kontakt�w pomi�dzy grupami
  ONCHECK_CHANGENAME = 4; // Wtyczka pozwala na zmian� nazwy kontaktu
  ONCHECK_SENDMESSAGE = 5; // Wtyczka pozwala na wysy�anie wiadomo�ci
  ONCHECK_SENDIMAGE = 6; // Wtyczka pozwala na wysy�k� obrazk�w
  ONCHECK_SETGROUPS = 7; // Wtyczka pozwala na edycje grup
  ONCHECK_VCARD = 8; // Wtyczka posiada mo�liwo�� edycji wizyt�wek

const //##019
  CHAT_NONE = 0; // Stan u�ytkownika nieznany
  CHAT_ACTIVE = 1; // U�ytkownik ma aktywne okno rozmowy z odbiorc�
  CHAT_COMPOSING = 2; // U�ytkownik pisz� wiadomo��
  CHAT_GONE = 3; // U�ytkownik odszed� / wy��czy� okno rozmowy z odbiorc�
  CHAT_INACTIVE = 4; // U�ytkownik nie aktywny
  CHAT_PAUSED = 5; // Uzytkownik na chwil� przesta� pisa� wiadomo��

const //##020 // sta�e stanu wiadomo�ci SMS
  SMS_STATUS_FAILED = 0; // SMS nie wys�any
  SMS_STATUS_OK = 1; // SMS wys�any
  SMS_STATUS_DELIVERED = 2; // SMS dostarczono
  SMS_STATUS_NOTDELIVERED = 3; // SMS nie dostarczony

const //##021
  SMS_SEND_OK = 1; // SMS wys�ano prawid�owo
  SMS_SEND_FAILED = 2; // SMS nie uda�o si� wys�a�
  SMS_SEND_FAILED_SET = 3; // SMS nie uda�o si� wys�a�, b��dna konfiguracja
  SMS_SEND_FAILED_AUTH = 4; // SMS nie uda�o si� wys�a�, brak autoryzacji
  SMS_SEND_FAILED_CONNECT = 5; // SMS nie uda�o si� wys�a�, brak po��czenia
  SMS_SEND_FAILED_LIMIT = 6; // SMS nie uda�o si� wys�a�, limit wiadomo�ci przekroczono
  SMS_SEND_FAILED_TOKEN = 7; // SMS nie uda�o si� wys�a�, b��dny token
  SMS_SEND_FAILED_NOSUPPORT = 8; // SMS nie uda�o si� wys�a�, bramka nie obs�uguj� nr. telefonu
  SMS_SEND_FAILED_UNKNOWN = 9; // SMS nie uda�o si� wys�a�, przyczyna nieznana
  SMS_SEND_OK_SILENT = 10; // SMS wys�ano prawid�owo, nie pokazuj komunikat�w
  SMS_SEND_OK_SILENT_NOENABLE = 11; // SMS wys�ano prawid�owo, kontrolki SMS zostaj� jednak wci�� nieaktywne.
  SMS_SEND_FAILED_FILTER = 12; // SMS nie uda�o si� wys�a�, wiadomo�� zawiera niedozwolone znaki/s�owa
  SMS_SEND_FAILED_TOOLONG = 13; // SMS nie uda�o si� wys�a�, wiadomo�� zbyt d�uga
  SMS_SEND_FAILED_ABORTED = 14; // SMS nie uda�o si� wys���, u�ytkownik wycofa� si�

const //##022 // sta�e d�wi�kowe
  SOUND_FIRSTIN = 0; // D�wi�k pierwszej przychodz�cej wiadomo�ci
  SOUND_IN = 1; // D�wi�k przychodz�cej wiadomo�ci
  SOUND_OUT = 2; // D�wi�k wychodz�cej wiadomo�ci
  SOUND_STATUS = 3; // D�wi�k zmiany stanu
  SOUND_TRANSFER = 4; // D�wi�k nowego transferu
  SOUND_ENDTRANSFER = 5; // D�wi�k zako�czonego transferu
  SOUND_LOCK = 6; // D�wi�k zabezpieczenia AQQ
  SOUND_HELP = 7; // D�wi�k pomocy
  SOUND_CLOSEHELP = 8; // D�wi�k zamkni�cia pomocy
  SOUND_CHATIN = 9; // D�wi�k przychodz�cej wiadomo�ci na czacie
  SOUND_CHATOUT = 10; // D�wi�k wychodz�cej wiadomo�ci na czacie
  SOUND_ATTENTION = 11; // D�wi�k proszenia o uwag�
  SOUND_CALLIN = 12; // D�wi�k przychodz�cej rozmowy g�osowej
  SOUND_CALLEND = 13; // D�wi�k zako�czonej rozmowy g�osowej
  SOUND_INFOBOX = 14; // D�wi�k funkcji "Daj mi zna�"
  SOUND_POINTS = 15; // D�wi�k awansu na wy�szy poziom
  SOUND_AUTH = 16; // D�wi�k autoryzacji
  SOUND_NEWS = 17; // D�wi�k powiadomienia
  SOUND_CLICK = 18; // D�wi�k klikni�cia myszk� komputerow�

const //##023
  IMP_TYPE_EXPORT = 0; // Eksport
  IMP_TYPE_IMPORT = 1; // Import
  IMP_TYPE_OTHER = 2; // Inne

const //##024
  ALIGN_TOP = 0; // Rozci�gni�cie do g�ry
  ALIGN_BOTTOM = 1; // Rozci�gni�cie do do�u
  ALIGN_LEFT = 2; // Rozci�gni�cie do lewej strony
  ALIGN_RIGHT = 3; // Rozci�gni�cie do prawej strony
  ALIGN_CLIENT = 4; // Rozci�gni�cie na ca�y dost�pny obszar
  ALIGN_TOWINDOW = 5;

const //##025
  MSGKIND_CHAT = 0; // Wiadomo�� w oknie rozmowy
  MSGKIND_GROUPCHAT = 1; // Wiadomo�� na czacie
  MSGKIND_RTT = 2; // Wiadomo�� typu RTT

const //##026
  TRANSFER_DECLINE = 0; // Transfer odrzocono
  TRANSFER_ACCEPT = 1; // Transfer zaakceptowano
  TRANSFER_CONNECTING = 2; // Trwa ��czenie
  TRANSFER_ACTIVE = 3; // Transfer w toku
  TRANSFER_DISCONNECT = 4; // Transfer roz��czony
  TRANSFER_FINISHED = 5; // Transfer zako�czony pomy�lnie
  TRANSFER_PAUSED = 6; // Pauza
  TRANSFER_ABORTED = 7; // Zarzucono transfer w trakcie jego trwania

const //##027
  WINDOW_EVENT_CREATE = 1; // Okno zosta�o stworzone
  WINDOW_EVENT_CLOSE = 2; // Okno zosta�o zamkni�te

const //##028
  SUB_BOTH = 0; // Subskrypcja w obie strony
  SUB_NONE = 1; // Brak subskrypcji
  SUB_FROM = 2; // Subskrypcja od u�ytkownika
  SUB_TO = 3; // Subskryocja do u�ytkownika
  SUB_REMOVE = 4; // Usuni�cie subskrypcji
  SUB_NONEASK = 5; // Brak odpowiedzi
  SUB_FROMASK = 6; // Zapytanie o subskrypcje

const //##029 // sta�e proxy
  SOCKS_5 = 0; // Wersja proxy 5
  SOCKS_4A = 1; // Wersja proxy 4A
  SOCKS_4 = 2; // Wersja proxy 4

/// # Structs - struktury
type
  THintEvent =
    (theAuth, // Autoryzacja
    theTitle, // Tytu�
    theData, // Data
    theStatus, // Status
    theActivity); // Ostatnia aktywno��

type
  TMiniEvent =
    (tmeDeleted, // Zdarzenie usuni�to
    tmeStatus, // Status
    tmePseudoStatus, // Wygl�d statusu, ale nie traktowane jak status
    tmePseudoMsg, // Wygl�d wiadomo�ci, ale nie traktowane jako wiadomo��
    tmeMsg,  // Wiadomo��
    tmePseudoMsgCap, // Wygl�d tytu�u wiadomo�ci, ale nie traktowane jak tytu� wiadomo�ci
    tmeMsgCap, // Tytu� wiadomo�ci
    tmeInfo,   // Informacja
    tmeAction, // Akcja
    tmeAbuse,  // Informacja o spimie
    tmeVoip);  // Rozmowa g�osowa

type
  TAddonType =
    (tatUnCheck, // Dodatek jeszcze nie sprawdzony
    tatUnknown, // Dodatek nieznanego typu
    tatVisualStyle, // Dodatek to wizualna kompozycja
    tatSmileys, // Dodatek to paczka emotek
    tatPlugin); // Dodtek to wtyczka

type
  PPluginChatPrep = ^TPluginChatPrep;

  TPluginChatPrep = record
    cbSize: Integer; // Wielko�� struktury
    UserIdx: Integer; // ID sieci
    JID: PWideChar; // Pe�ny JID kana�u
    Channel: PWideChar; // Nazwa kana�u do wy�wietlenia u�ytkownikowi
    CreateNew: Boolean; // Tworzymy nowy kana� na serwerze [tak/nie]
    Fast: Boolean; // Szybkie "OK", z domy�lnymi ustawieniami.
  end;

type
  PPluginChatPresence = ^TPluginChatPresence;

  TPluginChatPresence = record
    cbSize: Integer; // Wielko�� struktury
    UserIdx: Integer; // ID sieci
    JID: PWideChar; // JabberID na czacie
    RealJID: PWideChar; // Prawdziwe JabberID
    Affiliation: PWideChar; // Grupa np. moderatorzy, zbanowani itd. ##007
    Role: PWideChar; // Rola ##006
    Offline: Boolean; // Po��czony z pokojem tak/nie
    Nick: PWideChar; // Pseudonim
    Kicked, Banned: Boolean; // Wykopany, zbanowany u�ytkownik
    Ver: PWideChar; // Wersja
    JIDToShow: PWideChar; // JID kt�ry nale�y pokaza�
  end;

type
  PPluginChatOpen = ^TPluginChatOpen;

  TPluginChatOpen = record
    cbSize: Integer; // Wielko�� struktury
    JID: PWideChar; // JabberID
    UserIdx: Integer; // ID sieci
    Channel: PWideChar; // Nazwa kana�u
    Nick: PWideChar; // Pseudonim
    NewWindow: Boolean; // Nowe okno tak/nie
    IsNewMsg: Boolean; // Jest dost�pna nowa wiadomo�� tak/nie
    Priority: Boolean; // Otw�rz priorytetowo
    OriginJID: PWideChar; // Prawdziwy JID
    ImageIndex: Integer; // Index ikony
    AutoAccept: Boolean; // Automatycznie akceptuj zaproszenie do czat-u
    ChatMode: Byte; // Tryb czatu ##005
  end;

type
  PPluginHook = ^TPluginHook;

  TPluginHook = record
    HookName: PWideChar; // Nazwa uchwytu notyfikacji
    wParam: DWORD; // Parametr wParam
    lParam: DWORD; // Parametr lParam
  end;

type
  PSaveSetup = ^TSaveSetup;

  TSaveSetup = record
    Section: PWideChar; // Sekcja w pliku INI
    Ident: PWideChar; // Identyfikator pola w pliku INI
    Value: PWideChar; // Zawarto�� pola
  end;

type
  PPluginSong = ^TPluginSong;

  TPluginSong = record
    Title: PWideChar; // Tytu� piosenki
    Position: Integer; // Pozacja na playli�cie numeryczna
    Length: Integer; // D�ugo�� piosenki w sekundach
  end;

type
  PPluginAddForm = ^TPluginAddForm;

  TPluginAddForm = record
    UserIdx: Integer; // ID sieci
    JID: PWideChar; // JabberID
    Nick: PWideChar; // Pseudonim
    Agent: PWideChar; // Agent serwera
    Modal: Boolean; // Okno modalne tak/nie
    Custom: Boolean; // Nie bierz pod uwag� standardowych agent�w
  end;

type
  PPluginShowInfo = ^TPluginShowInfo;

  TPluginShowInfo = record
    cbSize: Integer; // Wielko�� struktury
    Event: TMiniEvent; // Typ zdarzenia
    Text: PWideChar; // Tre��
    ImagePath: PWideChar; // �cie�ka do ikony
    TimeOut: Integer; // Czas wyga�ni�cia w milisekundach
    ActionID: PWideChar; // Akcja do ewentualnego uruchomienia po klikni�ciu
    Tick: Cardinal; // Tick z funkcji systemowej GetTickCount
  end;

type
  PPluginToolTipItem = ^TPluginToolTipItem;

  TPluginToolTipItem = record
    cbSize: Integer; // Wielko�� struktury
    Tick: Cardinal; // Tick z funkcji systemowej GetTickCount
    Event: Integer; // Typ zdarzenia ##009
    Text: PWideChar; // Tre��
    ImagePath: PWideChar; // �cie�ka do ikony
    ActionID: PWideChar; // ID akcji
    Y1, Y2: Integer; // Pozycja na ekranie
    TimeOut: Integer; // Czas wyga�ni�cia w milisekundach
    Flag: Integer; // Dodatkowe flagi
  end;

type
  PPluginToolTipID = ^TPluginToolTipID;

  TPluginToolTipID = record
    cbSize: Integer; // Wielko�� struktury
    ID: PWideChar; // ID tooltipa
    Name: PWideChar; // Nazwa tooltipa
  end;

type
  PPluginSMSResult = ^TPluginSMSResult;

  TPluginSMSResult = record
    cbSize: Integer; // Wielko�� struktury
    ID: PWideChar; // ID wys�anej wiadomo�ci
    Result: Integer; // Wynik wysy�ki
  end;

type
  PPluginXMLChunk = ^TPluginXMLChunk;

  TPluginXMLChunk = record
    cbSize: Integer; // Wielko�� struktury
    ID: PWideChar; // ID
    From: PWideChar; // Sk�d pochodzi pakiet
    XML: PWideChar; // Pakiet XML
    UserIdx: Integer; // ID sieci
  end;

type
  PPluginContactSimpleInfo = ^TPluginContactSimpleInfo;

  TPluginContactSimpleInfo = record
    cbSize: Integer; // Wielko�� struktury
    JID: PWideChar; // Jabber ID
    First: PWideChar; // Pierwsze imie
    Last: PWideChar; // Nazwisko
    Nick: PWideChar; // Pseudonim
    CellPhone: PWideChar; // Tel. kom�rkowy
    Phone: PWideChar; // Telefon stacjonarny
    Mail: PWideChar; // Adres e-mail
  end;

type
  PPluginItemDescriber = ^TPluginItemDescriber;

  TPluginItemDescriber = record
    cbSize: Integer; // Wielko�� struktury
    FormHandle: Cardinal; // Uchwyt okna (zero - g��wne okno)
    ParentName: PWideChar; // Nazwa rodzica kt�rego potomkiem jest element
    Name: PWideChar; // Nazwa elementu
  end;

type
  PPluginPopUp = ^TPluginPopUp;

  TPluginPopUp = record
    cbSize: Integer; // Wielko�� struktury
    Name: PWideChar; // Nazwa menu
    ParentHandle: Cardinal; // Uchyt rodzica
    Handle: Cardinal; // Uchwyt
  end;

type
  PPluginTriple = ^TPluginTriple;

  TPluginTriple = record
    cbSize: Integer; // Wielko�� struktury
    Handle1: Cardinal; // Zmienna pomocnicza 1
    Handle2: Cardinal; // Zmienna pomocnicza 2
    Handle3: Cardinal; // Zmienna pomocnicza 3
    Param1: Integer; // Zmienna pomocnicza 4
    Param2: Integer; // Zmienna pomocnicza 5
  end;

type
  PPluginError = ^TPluginError;

  TPluginError = record
    cbSize: Integer; // Wielko�� struktury
    ID: PWideChar; // ID zdarzenia
    Desc: PWideChar; // Opis
    LangID: PWideChar; // ID lokalizacji j�zykowej
    ErrorCode: PWideChar; // Kod b��du
  end;

type
  PPluginTransfer = ^TPluginTransfer;

  TPluginTransfer = record
    cbSize: Integer; // Wielko�� struktury
    ID: PWideChar; // ID transferu
    FileName: PWideChar; // Nazwa pliku
    Location: PWideChar; // Lokalizacja pliku
    Status: Integer; // Status przesy�u ##026
    TextInfo: PWideChar; // Informacja tekstowa
    FileSize: Int64; // Wielko�� pliku
    Position: Int64; // Aktualna pozycja w zapisie/odczycie
    LastDataSize: Int64; // Ostatnia wielko�� otrzymanego/wys�anego pakietu
  end;

type
  PPluginTransferOld = ^TPluginTransferOld;

  TPluginTransferOld = record
    cbSize: Integer; // Wielko�� struktury
    ID: PWideChar; // ID transferu
    FileName: PWideChar;  // Nazwa pliku
    Location: PWideChar; // Lokalizacja pliku
    Status: Integer; // Status przesy�u ##026
    TextInfo: PWideChar; // Tekstowa informacja
    FileSize: Cardinal; // Wielko�� pliku
    Position: Cardinal; // Aktualna pozycja w zapisie
    LastDataSize: Cardinal; // Ostatnia wielko�� otrzymanego/wys�anego pakietu
  end;

type
  PPluginWindowEvent = ^TPluginWindowEvent;

  TPluginWindowEvent = record
    cbSize: Integer; // Wielko�� struktury
    ClassName: PWideChar; // Klasa okna
    Handle: Cardinal; // Uchwyt okna
    WindowEvent: Integer; // Zdarzenie ##027
  end;

type
  PPluginChatState = ^TPluginChatState;

  TPluginChatState = record
    cbSize: Integer; // Wielko�� struktury
    Text: PWideChar; // Tekst
    Length: Integer; // D�. tekstu
    SelStart: Integer; // Zaznaczenie start
    SelLength: Integer; // Zaznaczenie stop
    ParentHandle: Cardinal; // Uchwyt rodzica
    Handle: Cardinal; // Uchwyt
    ChatState: Integer; // sta�e pisaka ##019
  end;

type
  PPluginWebItem = ^TPluginWebItem;

  TPluginWebItem = record
    cbSize: Integer; // Wielko�� struktury
    ID: PWideChar; // ID zawarte w kodzie HTML
    Text: PWideChar; // Zawarto�� tekstowa
  end;

type
  PPluginSmallInfo = ^TPluginSmallInfo;

  TPluginSmallInfo = record
    cbSize: Integer; // Wielko�� struktury
    Text: PWideChar; // Tekst
  end;

type
  PPluginWebBeforeNavEvent = ^TPluginWebBeforeNavEvent;

  TPluginWebBeforeNavEvent = record
    cbSize: Integer; // Wielko�� struktury
    Handle: Cardinal; // Uchwyt do kontrolki IE
    URL: PWideChar; // Adres URL
    Flags: PWideChar; // Flagi
    TargetFrameName: PWideChar; // Ramka
    PostData: PWideChar; // Dane dla akcji POST
    Headers: PWideChar; // Nag��wki
  end;

type
  PPluginWebBrowser = ^TPluginWebBrowser;

  TPluginWebBrowser = record
    cbSize: Integer; // Wielko�� struktury
    Handle: Cardinal; // Uchwyt do kontrolki IE (0 je�eli tworzymy now�)
    Top, Left, Width, Height: Integer; // Pozycja
    Align: Integer; // Auto rozci�ganie (typ) ##024
    RegisterAsDropTarget: Boolean; // Zarejestruj jako drop-target
    SetVisible: Boolean; // Kontroka IE widoczna tak/nie
    SetEnabled: Boolean; // Kontrolka IE aktywna tak/nie
  end;

type
  PPluginDebugInfo = ^TPluginDebugInfo;

  TPluginDebugInfo = record
    cbSize: Integer; // Wielko�� struktury
    JID: PWideChar; // JabberID
    XML: PWideChar; // Pakiet XML
    Mode: Byte; // Tryb do/od ##017
  end;

type
  PPluginMaxStatus = ^TPluginMaxStatus;

  TPluginMaxStatus = record
    cbSize: Integer; // Wielko�� struktury
    IconIndex: Integer; // Ikona sieci
    Name: PWideChar; // Nazwa
    Max: Integer; // Maksymalna d�ugo�c opisu tekstowego
  end;

type
  PPluginProxy = ^TPluginProxy;

  TPluginProxy = record
    cbSize: Integer; // Wielko�� struktury
    ProxyEnabled: Boolean; // Proxy aktywne tak/nie
    ProxyServer: PWideChar; // Adres serwera proxy
    ProxyPort: Integer; // Adres portu
    ProxyType: Integer; // Typ Proxy (sta�e proxy ##029)
    ProxyAuth: Boolean; // Proxy wymaga autoryzacji
    ProxyLogin: PWideChar; // Login
    ProxyPass: PWideChar; // Has�o
  end;

type
  PPluginImpExp = ^TPluginImpExp;

  TPluginImpExp = record
    cbSize: Integer; // Wielko�� struktury
    ImportType: Integer; // Tym import / eskport / inne ##023
    Name: PWideChar; // Nazwa
    Service: PWideChar; // Serwis do uruchomienia we wtyczce
    IconIndex: Integer; // Przypisany index ikony
  end;

type
  PPluginAutomation = ^TPluginAutomation;

  TPluginAutomation = record
    cbSize: Integer; // Wielko�� struktury
    Flags: Integer; // Flagi
    NewState: Integer; // Nowy stan
    LastActive: Double; // Ostatnia aktywno��
  end;

type
  PPluginStateChange = ^TPluginStateChange;

  TPluginStateChange = record
    cbSize: Integer; // Wielko�� struktury
    OldState: Integer; // Poprzedni stan
    NewState: Integer; // Nowy stan
    Status: PWideChar; // Status tekstowy
    ByHand: Boolean; // U�ytkownik zmienia status r�cznie
    UserIdx: Integer; // ID sieci skojarzonej
    JID: PWideChar; // JabberID
    Force: Boolean; // Zmiana przymusowa
    Server: PWideChar; // Adres serwera
    Authorized: Boolean; // U�ytkownik jest zalogowany tak/nie
    FromPlugin: Boolean; // Request pochodzi z wtyczki sieciowej tak/nie
    Resource: PWideChar; // Zas�b
  end;

type
  PPluginSMS = ^TPluginSMS;

  TPluginSMS = record
    cbSize: Integer; // Wielko�� struktury
    CellPhone: PWideChar; // Numer telefonu
    Msg: PWideChar; // Wiaodmo�� SMS
    Sign: PWideChar; // Podpis
    GateID: Integer; // ID bramki SMS
  end;

type
  PPluginTwoFlagParams = ^TPluginTwoFlagParams;

  // Struktura wykorzystywana w kilku miejscach - pomocnicza
  TPluginTwoFlagParams = record
    cbSize: Integer; // Wielko�c struktury
    Param1: PWideChar; // Parametr pomocniczy 1
    Param2: PWideChar; // Parametr pomocniczy 2
    Flag1: Integer; // Parametr pomocniczy 3
    Flag2: Integer; // Parametr pomocniczy 4
  end;

type
  PPluginAddUser = ^TPluginAddUser;

  TPluginAddUser = record
    cbSize: Integer; // Wielko�� struktury
    UID: PWideChar; // UID/JID kontaktu
    Server: PWideChar; // Serwer
    Nick: PWideChar; // Pseudonim
    Group: PWideChar; // Grupy
    Reason: PWideChar; // Pow�d dodania
    Service: PWideChar; // Serwis z kt�rego korzysta u�ytkownik w celu dodania kontaktu
  end;

type
  PPluginSMSGate = ^TPluginSMSGate;

  TPluginSMSGate = record
    cbSize: Integer; // Wielko�� struktury
    Name: PWideChar; // Nazwa
    Prompt: PWideChar; // Kr�tka informacja dla u�ytkownika dotycz�ca obs�ugi
    IsConfig: Boolean; // Bramka posiada mo�liwo�� dodatkowej konfiguracji
    MaxLength: Integer; // Maksymalna d�ugo�� wiadomo�ci w bramce SMS
    SignMaxLength: Integer; // Maksymalna d�ugo�� podpisu w bramce SMS
  end;

type
  PPluginAgent = ^TPluginAgent;

  TPluginAgent = record
    cbSize: Integer; // Wielko�� struktury
    JID: PWideChar; // JabberID
    Name: PWideChar; // Nazwa funkcjonalno�ci serwera Jabberowego (agent)
    Prompt: PWideChar; // Etykieta przy rejestracji
    Transport: Boolean; // Agent to transport tak/nie
    Search: Boolean; // Agent to wyszukiwarka kontakt�w tak/nie
    Groupchat: Boolean; // Agent to czaty (MUC)
    Agents: Boolean; // Posiada agent�w tak/nie
    Service: PWideChar; // Nazwa serwisu
    CanRegister: Boolean; // Mo�na si� rejestrowa� tak/nie
    Description: PWideChar; // Opis
    RequiredID: Boolean; // Wymaga ID
    IconIndex: Integer; // Index ikony skojarzonej z agentem
    PluginAccountName: PWideChar; // Nazwa konta np. Sie� GG
  end;

type
  PPluginAvatar = ^TPluginAvatar;
  TPluginAvatar = record
    FileName: PWideChar; // �cie�ka do pliku
    XEPEmpty: Boolean; // Pusty awatar tak/nie
    SilentMode: Boolean; // Nie wy�wietlaj �adnych komunikat�w
    JID: PWideChar; // JID konta
  end;

type
  PPluginMessage = ^TPluginMessage;

  TPluginMessage = record
    cbSize: Integer; // Wielko�� struktury
    JID: PWideChar; // JabberID
    Date: Double; // Data
    ChatState: Integer; // Stan pisaka ##019
    Body: PWideChar; // Tre�� wiadomo�ci ##012
    Offline: Boolean; // Wiadomo�� typu offline
    DefaultNick: PWideChar; // Domy�lny nick skojarzony z wiadomo�ci�
    Store: Boolean; // Zachowana na dysku tak/nie
    Kind: Byte; // Rodzaj wiadomo�ci ##025
    ShowAsOutgoing: Boolean; // Poka� wiadomo�� jako wychodz�c�
  end;

type
  PPluginMsgPic = ^TPluginMsgPic;

  TPluginMsgPic = record
    cbSize: Integer; // Wielko�� struktury
    FilePath: PWideChar; // �cie�ka do pliku
    Description: PWideChar; // Opis
    ID: PWideChar; // ID
  end;

type
  PPluginFileTransfer = ^TPluginFileTransfer;

  TPluginFileTransfer = record
    cbSize: Integer; // Wielko�� struktury
    FilePath: PWideChar; // �cie�ka do pliku
    Description: PWideChar; // Opis
    ID: PWideChar; // ID
  end;

type
  PPluginInfo = ^TPluginInfo;

  TPluginInfo = record
    cbSize: Integer; // Wielko�� struktury
    ShortName: PWideChar; // Kr�tka nazwa wtyczki
    Version: DWORD; // Wersja wtyczki
    Description: PWideChar; // Kr�tki opis wtyczki
    Author: PWideChar; // Imie i nazwisko autora
    AuthorMail: PWideChar; // Kontakt do autora (e-mail)
    Copyright: PWideChar; // Do kogo nale�� prawa autorskie
    Homepage: PWideChar; // Strona domowa projektu
    Flag: Byte; // Dodatkowe flagi
    ReplaceDefaultModule: Integer; // Nie u�ywane
  end;

type
  PPluginActionEdit = ^TPluginActionEdit;

  TPluginActionEdit = record
    cbSize: Integer; // Wielko�� struktury
    pszName: PWideChar; // Nazwa akcji
    Caption: PWideChar; // Etykieta
    Hint: PWideChar; // Hint, pomoc.
    Enabled: Boolean; // Akcja aktywna tak/nie
    Visible: Boolean; // Akcja widoczna tak/nie
    IconIndex: Integer; // Index ikony
    Checked: Boolean; // Zaznaczenie tak/nie
  end;

type
  PPluginAccountInfo = ^TPluginAccountInfo;
  TPluginAccountInfo = record
    cbSize: Integer; // Wielko�� struktury
    Name: PWideChar;  // Nazwa konta
    JID: PWideChar; // JabberID
    Status: PWideChar; // Status tekstowy
    ShowType: Integer; // Status
    IconIndex: Integer;  // Aktualny index ikony stanu
  end;

type
  PPluginWindowStatus = ^TPluginWindowStatus;

  TPluginWindowStatus = record
    cbSize: Integer; // Wielko�� struktury
    Status: Integer;  // Status
    TextStatus: PWideChar; // Status tekstowy
    AllAccounts: Boolean; // Zastosuj dla wszystkich kont tak/nie
    OnlyNote: Boolean; // Zmie� tylko opis tak/nie
  end;

type
  PPluginAction = ^TPluginAction;

  TPluginAction = record
    cbSize: Integer; // Wielko�� struktury
    Action: PWideChar; // Nazwa akcji
    pszName: PWideChar; // Nazwa unikatowa
    pszCaption: PWideChar; // Etykieta
    Flags: DWORD; // Dodatkowe flagi
    Position: Integer; // Index pozycji w menu
    IconIndex: Integer; // Index ikony
    pszService: PWideChar; // Nazwa serwisu do uruchomienia
    pszPopupName: PWideChar; // Nazwa menu nadrz�dnego
    popupPosition: Integer; // Pozycja w menu nadrz�dnym
    hotKey: DWORD; // HotKey
    pszContactOwner: PWideChar;
    GroupIndex: Integer; // Index grupy
    Grouped: Boolean; // Czy w grupie tak/nie
    AutoCheck: Boolean; // Auto zaznaczanie tak/nie
    Checked: Boolean; // Zaznaczenie aktywne tak/nie
    Handle: Cardinal; // Uchwyt
    ShortCut: PWideChar; // Skr�t
    Hint: PWideChar; // Pomoc, hint.
    PositionAfter: PWideChar; // Ustaw w menu po podanej innej akcji
  end;

type
  PPluginAccountEvents = ^TPluginAccountEvents;

  TPluginAccountEvents = record
    cbSize: Integer; // Wielko�� struktury
    DisplayName: PWideChar; // Nazwa konta
    IconIndex: Integer; // Index ikony
    EventNew: Boolean; // Mo�na tworzy� nowe konta tak/nie
    EventEdit: Boolean; // Mo�na edytowa� konto tak/nie
    EventDelete: Boolean; // Mo�na usuwa� konto tak/nie
    EventPassChange: Boolean; // Mo�na zmienia� has�o tak/nie
    EventDefault: Boolean; // Domy�lny event np. uruchomienie ustawie� konta tak/nie
  end;

type
  PPluginContact = ^TPluginContact;

  TPluginContact = record
    cbSize: Integer; // Wielko�� struktury
    JID: PWideChar; // JabberID kontaktu
    Nick: PWideChar; // Pseudonim
    Resource: PWideChar; // Zas�b z kt�rego ��czy si� kontakt
    Groups: PWideChar; // Grupy do kt�rych przynale�y kontakt
    State: Integer; // Status kontaktu ##016
    Status: PWideChar; // Status opisowy kontaktu
    Temporary: Boolean; // Kontakt tymczasowy (nie jest zapisany na li�cie kontakt�w)
    FromPlugin: Boolean;  // Kontakt pochodzi z wtyczki tak/nie
    UserIdx: Integer; // ID sieci do kt�rej przynale�y kontakt
    Subscription: Byte; // Rodzaj subskrybcji ##028
    IsChat: Boolean; // Kontakt czatowy tak/nie
  end;

type
  PPluginNewsData = ^TPluginNewsData;
  TPluginNewsData = record
    Kind: PWideChar; // Unikatowy identyfikator (sta�y)
    Title: PWideChar; // Nazwa �r�d�a powiadomie�
    ID: PWideChar; // Unikatowe ID
    Active: Boolean; // �r�d�o aktywne tak/nie
  end;

type
  PPluginNewsItem = ^TPluginNewsItem;

  TPluginNewsItem = record
    Date: TDatetime; // Data publikacji powiadomienia
    News: PWideChar; // Tre�� powiadomienia
    Title: PWideChar; // Tytu� powiadomienia
    Source: PWideChar;  // �r�d�o np. link URL
    ParentIndex: Integer; // ID otrzymane w notyfikacji
  end;

type
  PAQQHook = ^TAQQHook;
  TAQQHook = function(wParam, lParam: DWORD): Integer; stdcall; // Uchwyt do notyfikacji

type
  PAQQService = ^TAQQService;
  TAQQService = function(wParam, lParam: DWORD): Integer; stdcall; // Funkcja serwisowa

type
  PPluginLink = ^TPluginLink;

  TPluginLink = record
    Path: PWideChar; // Wska�nik do �cie�ki (razem z nazw� pliku), wczytanej wtyczki.
    CreateHookableEvent: function(Name: PWideChar): THandle; stdcall;
    DestroyHookableEvent: function(hEvent: THandle): Integer; stdcall;
    NotifyEventHooks: function(hEvent: THandle; wParam, lParam: DWORD): Integer; stdcall;
    HookEvent: function(Name: PWideChar; HookProc: TAQQHook): THandle; stdcall; // Funkcja pod��czaj�ca interfejs wtyczki pod wskazan� notyfikacj� w interfejsie AQQ.
    HookEventMessage: function(Name: PWideChar; Handle: HWND; Msg: Cardinal): THandle; stdcall;
    UnhookEvent: function(hHook: THandle): Integer; stdcall; // Funkcja od��czaj�ca notyfikacje w interfejsie AQQ od interfejsu wtyczki.
    CreateServiceFunction: function(Name: PWideChar; ServiceProc: TAQQService): THandle; stdcall; // Tworzy funkcj� serwisow� (serwis).
    CreateTransientServiceFunction: Pointer;
    DestroyServiceFunction: function(hService: THandle): Integer; stdcall; // Niszczy funkcj� serwisow� (serwis).
    CallService: function(Name: PWideChar; wParam, lParam: DWORD): Integer; stdcall; // Wywo�uj� serwis.
    ServiceExists: function(Name: PWideChar): Integer; stdcall;
  end;

/// @---------------------------------------------------------------------------

const
  CALLSERVICE_NOTFOUND = $80000000;
  MAXMODULELABELLENGTH = 64;

type
  TVersionInfo = record
    a, b, c, d: Integer end;

  type
    TAQQPluginInfo = function(AQQVersion: DWORD): PPluginInfo; stdcall;
    TLoad = function(Link: PPluginLink): Integer; stdcall;
    TUnload = function: Integer; stdcall;
    TSettings = function: Integer; stdcall;

    // ------------------------------------------------------------------------------

  function PLUGIN_MAKE_VERSION(a, b, c, d: Integer): DWORD;
  function PLUGIN_GET_VERSION(Ver: DWORD): TVersionInfo;
  function CompareVersion(vera, verb: DWORD): Integer;

implementation

function PLUGIN_MAKE_VERSION(a, b, c, d: Integer): DWORD;
begin
  Result := (((a and $FF) shl 24) or ((b and $FF) shl 16) or ((c and $FF) shl 8) or (d and $FF));
end;

function PLUGIN_GET_VERSION(Ver: DWORD): TVersionInfo;
begin
  Result.d := (Ver and $000000FF);
  Result.c := (Ver and $0000FF00) shr 8;
  Result.b := (Ver and $00FF0000) shr 16;
  Result.a := (Ver and $FF000000) shr 24;
end;

function CompareVersion(vera, verb: DWORD): Integer;
{ compares 2 versions. if versiona is newer than version b then the result is
  greater than 0, otherwise it's smaller }
begin
  Result := 0;
  if PLUGIN_GET_VERSION(vera).a > PLUGIN_GET_VERSION(verb).a then
    Inc(Result, 100);
  if PLUGIN_GET_VERSION(vera).a < PLUGIN_GET_VERSION(verb).a then
    Dec(Result, 100);

  if PLUGIN_GET_VERSION(vera).b > PLUGIN_GET_VERSION(verb).b then
    Inc(Result, 50);
  if PLUGIN_GET_VERSION(vera).b < PLUGIN_GET_VERSION(verb).b then
    Dec(Result, 50);

  if PLUGIN_GET_VERSION(vera).c > PLUGIN_GET_VERSION(verb).c then
    Inc(Result, 10);
  if PLUGIN_GET_VERSION(vera).c < PLUGIN_GET_VERSION(verb).c then
    Dec(Result, 10);

  if PLUGIN_GET_VERSION(vera).d > PLUGIN_GET_VERSION(verb).d then
    Inc(Result, 1);
  if PLUGIN_GET_VERSION(vera).d < PLUGIN_GET_VERSION(verb).d then
    Dec(Result, 1);
end;

end.

