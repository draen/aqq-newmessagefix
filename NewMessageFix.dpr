// Copyright (C) 2013 Rafa� Szczerbi�ski
//
// This file is part of LoLFormatter
//
// NewMessageFix is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3, or (at your option)
// any later version.
//
// NewMessageFix is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with GNU Radio; see the file COPYING. If not, write to
// the Free Software Foundation, Inc., 51 Franklin Street,
// Boston, MA 02110-1301, USA.

library NewMessageFix;

uses
  Windows,
  PluginAPI;

var
  PluginInfo: TPluginInfo;
  PluginLink: TPluginLink;
  Timer: THandle;
  Window: THandle;
  JID: String;


{$R *.res}

function OnMessageReceive(wParam, lParam: DWord): Integer; stdcall; forward;
function OnWindowOpen(wParam, lParam: DWord): Integer; stdcall; forward;

procedure OnTimer(Wnd: HWnd; Msg, Event: Cardinal; Time: DWord); stdcall;
begin
  PluginLink.CallService(AQQ_FUNCTION_EXECUTEMSG_NOPRIORITY, 0, DWord(PWideChar(WideString(JID))));
  JID := '';
  KillTimer(0, Timer);
end;

function AQQPluginInfo(AQQVersion: DWord): PPluginInfo; stdcall;
begin
  PluginInfo.cbSize := SizeOf(TPluginInfo);
  PluginInfo.ShortName := 'NewMessageFix';
  PluginInfo.Version := PLUGIN_MAKE_VERSION(1,0,0,6);
  PluginInfo.Description := 'Nowa wiadomo�� pojawia si� w otwartym oknie rozmowy.';
  PluginInfo.Author := 'Draen';
  PluginInfo.AuthorMail := 'draen94@gmail.com';
  PluginInfo.Copyright := 'Draen';
  PluginInfo.Homepage := 'http://draen.y0.pl';
  PluginInfo.Flag := 0;
  PluginInfo.ReplaceDefaultModule := 0;

  Result := @PluginInfo;
end;

function Load(Link: PPluginLink): Integer; stdcall;
begin
  PluginLink := Link^;
  PluginLink.HookEvent(AQQ_CONTACTS_RECVMSG, OnMessageReceive);
  PluginLink.HookEvent(AQQ_SYSTEM_WINDOWEVENT, OnWindowOpen);
  Result := 0;
end;

function Unload: Integer; stdcall;
begin
  Result := 0;
  PluginLink.UnhookEvent(THandle(@OnMessageReceive));
  PluginLink.UnhookEvent(THandle(@OnWindowOpen));
end;

function OnMessageReceive(wParam, lParam: DWord): Integer; stdcall;
var
  y: TPluginTriple;
begin
  Result := 0;
  y.cbSize := sizeof(TPluginTriple);
  y.Handle1 := Window;
  if (PluginLink.CallService(AQQ_FUNCTION_TABCOUNT, DWord(@y), 0) > 0) AND (PPluginContact(wParam)^.IsChat = false) then
  begin
    JID := PPluginContact(wParam)^.JID;
    Timer := SetTimer(0, 0, 100, @OnTimer);
  end;
end;

function OnWindowOpen(wParam, lParam: DWord): Integer; stdcall;
begin
  if PPluginWindowEvent(lParam)^.ClassName = 'TfrmSend' then
  begin
    if PPluginWindowEvent(lParam)^.WindowEvent = WINDOW_EVENT_CREATE then Window := PPluginWindowEvent(lParam)^.Handle
    else Window := 0;
  end;
  Result := 0;
end;

exports
  Load,
  Unload,
  AQQPluginInfo,
  OnMessageReceive;

begin
end.